<?php
add_action('web_to_lead_form_submitted','my_web_to_lead_form_submitted_redirect_to_custom_url');

function my_web_to_lead_form_submitted_redirect_to_custom_url($data){
    echo json_encode(array(
      'success'=>true,
      'redirect_url'=>'https://landingpages.planosdesaude360.com.br/b2b/obrigado.php'
    ));
    die;
}


