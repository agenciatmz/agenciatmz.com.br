<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="robots" content="noindex, nofollow" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>Agência TresMeiaZero</title>
    <meta name="description" content="" />
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
    <link href="https://fonts.googleapis.com/css?family=Khand:300,400,500,600,700" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="assets/css/demo.css" rel="stylesheet" />
    <link href="assets/css/formValidation.min.css" rel="stylesheet" />
    <script src="assets/js/plugins/sweetalert2.all.js"></script>

    <link href="assets/css/obrigado.css" rel="stylesheet" />
<!-- Google Code for lead tr&ecirc;smeiazero Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 823055622;
var google_conversion_label = "eV13CO7svXsQhqq7iAM";
var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/823055622/?label=eV13CO7svXsQhqq7iAM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>



<!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '139420260060569');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=139420260060569&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->
</head>


<body>


<div class="header-1">

    <div class="page-header header-filter">
        <div class="page-header-image" style="background-image: url('assets/img/LandingPagesB2B/bg14.jpg');"></div>
        <div class="content-center">
            <div class="container">
                <div class="row">

                    <div class="col-md-12 ml-auto mr-auto">
                        <img src="assets/img/LandingPagesB2B/logo-color.png" width="200px">

                        <h1 class="title">OBRIGADO</h1>
                        <h3 class="description">Em breve nossos consultores entrarão em contato</h3>
                        <div class="col-md-12  ml-auto mr-auto">
                            <a href="../index.html" class="btn btn-info btn-lg btn-round">Voltar</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<footer class="footer " data-background-color="black">
    <div class="container">
        <div class="copyright">
            ©
            <script>
                document.write(new Date().getFullYear())
            </script>, Desenvolvido por
            <a href="http://agenciatresmeiazero.com.br/home" target="_blank">#agênciatrêsmeiazero</a>.
        </div>
    </div>
</footer>
</body>
<script src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-2.1.3.min.js"></script>
<script type = "text/javascript" src = "https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/jquery-ui.min.js"></script>
<script src="assets/js/plugins/jquery.validate.min.js"></script>
<script src="assets/js/plugins/additional-methods.min.js"></script>
<script src="assets/js/plugins/sweet-scroll.min.js"></script>
<script src="assets/js/plugins/sweetalert2.all.js"></script>
<script>$(document).ready(function () {
        swal({ title: "Obrigado", text: "Em breve nossos consultores entrarão em contato\n", type: "success" });
    });
</script>
<script src="assets/js/validation.js"></script>
<script src="assets/js/plugins/jquery.mask.js" type="text/javascript"></script>
<script src="assets/js/plugins/api-estado-cidade.js"></script>
<script src="assets/js/plugins/custom.js"></script>
<script src="assets/js/core/popper.min.js" type="text/javascript"></script>
<script src="assets/js/core/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/js/plugins/moment.min.js"></script>
<script src="assets/js/plugins/bootstrap-switch.js"></script>
<script src="assets/js/plugins/bootstrap-tagsinput.js"></script>
<script src="assets/js/plugins/bootstrap-selectpicker.js" type="text/javascript"></script>
<script src="assets/js/plugins/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<script src="assets/js/now-ui-kit.js?v=1.2.0" type="text/javascript"></script>
</html>






