<?php

	/* Include all individual CPT. */
	$prefix_cpt = "cpt_";

	/* Case Study */
	require_once( $prefix_cpt . "casestudy.php" );

	/* Services */
	require_once( $prefix_cpt . "services.php" );
	
	/* Price Table */
	require_once( $prefix_cpt . "pricetable.php" );
?>