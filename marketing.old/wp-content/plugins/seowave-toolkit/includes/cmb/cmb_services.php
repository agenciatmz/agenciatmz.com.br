<?php
// Start with an underscore to hide fields from custom fields list
$prefix = 'ow_cf_';

$cmb_services = new_cmb2_box( array(
	'id'            => $prefix . 'metabox_portfolio',
	'title'         => __( 'Option', 'seowave' ),
	'object_types'  => array( 'ow_services', ), // Post type
	'context'       => 'normal',
	'priority'      => 'high',
	'show_names'    => true, // Show field names on the left
) );

$cmb_services->add_field( array(
	'name'         => __( 'Service Description', 'seowave' ),
	'id'           => $prefix . 'service_desc',
	'type'    => 'wysiwyg',
    'options' => array(
        'wpautop' => true, // use wpautop?
        'media_buttons' => true, // show insert/upload button(s)
        'textarea_rows' => get_option('default_post_edit_rows', 4), // rows="..."
        'tabindex' => '',
        'editor_css' => '', // intended for extra styles for both visual and HTML editors buttons, needs to include the `<style>` tags, can use "scoped".
        'editor_class' => '', // add extra class(es) to the editor textarea
        'teeny' => false, // output the minimal editor config used in Press This
        'dfw' => false, // replace the default fullscreen with DFW (needs specific css)
        'tinymce' => true, // load TinyMCE, can be used to pass settings directly to TinyMCE using an array()
        'quicktags' => true // load Quicktags, can be used to pass settings directly to Quicktags using an array()
    ),
) );

$cmb_services->add_field( array(
	'name'       => __( 'Service List', 'seowave' ),
	'id'         => $prefix . 'plan_features',
	'type'       => 'text',
	'repeatable' => true,
) );

$cmb_services->add_field( array(
	'name'       => __( 'Service Right image', 'seowave' ),
	'id'         => $prefix . 'plan_mainimage',
	'type'       => 'file',
	'repeatable' => true,
) );

$cmb_services->add_field( array(
	'name'       => __( 'Content Title', 'seowave' ),
	'id'         => $prefix . 'content_title',
	'type'       => 'text',
) );

$cmb_services->add_field( array(
	'name'         => __( 'Content Description', 'seowave' ),
	'id'           => $prefix . 'content_desc',
	'type'    => 'wysiwyg',
    'options' => array(
        'wpautop' => true, // use wpautop?
        'media_buttons' => true, // show insert/upload button(s)
        'textarea_rows' => get_option('default_post_edit_rows', 4), // rows="..."
        'tabindex' => '',
        'editor_css' => '', // intended for extra styles for both visual and HTML editors buttons, needs to include the `<style>` tags, can use "scoped".
        'editor_class' => '', // add extra class(es) to the editor textarea
        'teeny' => false, // output the minimal editor config used in Press This
        'dfw' => false, // replace the default fullscreen with DFW (needs specific css)
        'tinymce' => true, // load TinyMCE, can be used to pass settings directly to TinyMCE using an array()
        'quicktags' => true // load Quicktags, can be used to pass settings directly to Quicktags using an array()
    ),
) );

// $group_field_id is the field id string, so in this case: $prefix . 'demo'
$cmb_grp_services = $cmb_services->add_field( array(
	'id'          => $prefix . 'services_grp',
	'type'        => 'group',
	'options'     => array(
		'group_title'   => __( 'Content Box {#}', 'seowave' ), // {#} gets replaced by row number
		'add_button'    => __( 'Add Item', 'seowave' ),
		'remove_button' => __( 'Remove Item', 'seowave' ),
	),
) );

$cmb_services->add_group_field( $cmb_grp_services, array(
	'name' => 'Icon Image',
	'id'   => 'content_img',
	'type' => 'file',
) );

$cmb_services->add_group_field( $cmb_grp_services, array(
	'name' => 'Title',
	'id'   => 'keyword_title',
	'type' => 'text',  
) );

$cmb_services->add_field( array(
	'name'       => __( 'Ranking Image Title', 'seowave' ),
	'id'         => $prefix . 'ranking_title',
	'type'       => 'text',
) );

$cmb_services->add_field( array(
	'name' => 'Ranking  Image',
	'id'   => $prefix . 'ranking_img',
	'type' => 'file',
) );


// $group_field_id is the field id string, so in this case: $prefix . 'demo'
$cmb_grp_services = $cmb_services->add_field( array(
	'id'          => $prefix . 'services_grpresult',
	'type'        => 'group',
	'options'     => array(
		'group_title'   => __( 'Content Box {#}', 'seowave' ), // {#} gets replaced by row number
		'add_button'    => __( 'Add Item', 'seowave' ),
		'remove_button' => __( 'Remove Item', 'seowave' ),
	),
) );

$cmb_services->add_group_field( $cmb_grp_services, array(
	'name' => 'Result Icon Img',
	'id'   => 'result_img',
	'type' => 'file',
) );

$cmb_services->add_group_field( $cmb_grp_services, array(
	'name' => 'Result Value',
	'id'   => 'result_value',
	'type' => 'text',
) );

$cmb_services->add_group_field( $cmb_grp_services, array(
	'name' => 'Title',
	'id'   => 'result_title',
	'type' => 'text',  
) );
?>