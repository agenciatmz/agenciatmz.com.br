<?php
// Start with an underscore to hide fields from custom fields list
$prefix = 'ow_cf_';

/* Post : Gallery */
$cmb_post_gallery = new_cmb2_box( array(
	'id'            => $prefix . 'metabox_post_gallery',
	'title'         => __( 'Gallery Post Options', 'seowave' ),
	'object_types'  => array( 'post' ), // Post type
	'context'       => 'normal',
	'priority'      => 'high',
	'show_names'    => true, // Show field names on the left
) );

$cmb_post_gallery->add_field( array(
	'name' => __( 'Post Gallery', 'cmb2' ),
	'id'   => $prefix . 'post_gallery',
	'type' => 'file_list',
) );

/* Post : Quote */
$cmb_post_quote = new_cmb2_box( array(
	'id'            => $prefix . 'metabox_post_quote',
	'title'         => __( 'Quote Post Options', 'seowave' ),
	'object_types'  => array( 'post' ), // Post type
	'context'       => 'normal',
	'priority'      => 'high',
	'show_names'    => true, // Show field names on the left
) );

$cmb_post_quote->add_field( array(
	'name' => __( 'Post quote', 'cmb2' ),
	'id'   => $prefix . 'post_quote',
	'type'    => 'wysiwyg',
    'options' => array(
        'wpautop' => true, // use wpautop?
        'media_buttons' => true, // show insert/upload button(s)
        'textarea_rows' => get_option('default_post_edit_rows', 5), // rows="..."
        'tabindex' => '',
        'editor_css' => '', // intended for extra styles for both visual and HTML editors buttons, needs to include the `<style>` tags, can use "scoped".
        'editor_class' => '', // add extra class(es) to the editor textarea
        'teeny' => false, // output the minimal editor config used in Press This
        'dfw' => false, // replace the default fullscreen with DFW (needs specific css)
        'tinymce' => true, // load TinyMCE, can be used to pass settings directly to TinyMCE using an array()
        'quicktags' => true // load Quicktags, can be used to pass settings directly to Quicktags using an array()
    ),
) );

$cmb_post_quote->add_field( array(
	'name' => __( 'Author', 'cmb2' ),
	'id'   => $prefix . 'post_author',
	'type'    => 'text',
));

$cmb_post_quote->add_field( array(
	'name' => __( 'Quote Link', 'cmb2' ),
	'id'   => $prefix . 'post_author_link',
	'type'    => 'text_url',
));

/* Post : Layout */
$cmb_post_icon = new_cmb2_box( array(
	'id'            => $prefix . 'metabox_post_icon',
	'title'         => __( 'Post Icon Image', 'seowave' ),
	'object_types'  => array( 'post' ), // Post type
	'context'       => 'normal',
	'priority'      => 'high',
	'show_names'    => true, // Show field names on the left
) );

$cmb_post_icon->add_field( array(
	'name' => __( 'Icon Image', 'cmb2' ),
	'id'   => $prefix . 'post_icon',
	'type'    => 'file',
));
?>