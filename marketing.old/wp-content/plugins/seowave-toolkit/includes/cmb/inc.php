<?php
/**
 * Include and setup custom metaboxes and fields. (make sure you copy this file to outside the CMB directory)
 *
 * Be sure to replace all instances of 'yourprefix_' with your project's prefix.
 * http://nacin.com/2010/05/11/in-wordpress-prefix-everything/
 *
 * @category YourThemeOrPlugin
 * @package  Metaboxes
 * @license  http://www.opensource.org/licenses/gpl-license.php GPL v2.0 (or later)
 * @link     https://github.com/WebDevStudios/CMB2
 */

add_action( 'cmb2_init', 'register_ow_metabox' );
/**
 * Hook in and add a demo metabox. Can only happen on the 'cmb2_init' hook.
 */
function register_ow_metabox() {

	// Start with an underscore to hide fields from custom fields list
	$prefix = 'ow_cf_';

	/* ## Page/Post Options ---------------------- */

	/* - Styling Options */
	$cmb_styling = new_cmb2_box( array(
		'id'            => $prefix . 'metabox_styling',
		'title'         => __( 'Styling Options', "seowave"),
		'object_types'  => array( "page", "post" , "ow_casestudy", "ow_services"), // Post type
		'context'       => 'normal',
		'priority'      => 'high',
		'show_names'    => true, // Show field names on the left
	) );

	
	$cmb_styling->add_field( array(
		'name'             => 'Content Area Padding ?',
		'desc'             => 'If your content section need to have just after header area without space, please select an option Off',
		'id'               => $prefix . 'content_padding',
		'type'             => 'select',
		'default'          => 'on',
		'options'          => array(
			'on' => __( 'On', "seowave" ),
			'off'   => __( 'Off', "seowave" ),
		),
	) );

	$prefix_cmb = "cmb_";

	/* ## Product Options ---------------------- */
	require_once( $prefix_cmb . "product.php");

	/* ## Page Options ---------------------- */
	require_once( $prefix_cmb . "page.php");

	/* ## Post Options ---------------------- */
	require_once( $prefix_cmb . "post.php");
	
	/* ## Case Study Options ---------------------- */
	require_once( $prefix_cmb . "casestudy.php");
	
	/* ## Services Options ---------------------- */
	require_once( $prefix_cmb . "services.php");
	
	/* ## Price Table Options ---------------------- */
	require_once( $prefix_cmb . "pricetable.php");
}
?>