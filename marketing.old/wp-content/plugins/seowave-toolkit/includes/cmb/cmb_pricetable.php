<?php
// Start with an underscore to hide fields from custom fields list
$prefix = 'ow_cf_';

$cmb_pricetable= new_cmb2_box( array(
	'id'            => $prefix . 'metabox_pricetable',
	'title'         => __( 'Option', 'seowave' ),
	'object_types'  => array( 'ow_pricetable', ), // Post type
	'context'       => 'normal',
	'priority'      => 'high',
	'show_names'    => true, // Show field names on the left
) );

$cmb_pricetable->add_field( array(
	'name'         => __( 'Amount ', 'seowave' ),
	'id'           => $prefix . 'price_amt',
	'type'         => 'text',		
) );

$cmb_pricetable->add_field( array(
	'name'         => __( 'Cost For ', 'seowave' ),
	'id'           => $prefix . 'price_cost',
	'type'         => 'text',		
) );
/*
$cmb_pricetable->add_field( array(
    'name'       => __( 'Add Items Icon', seowave ),
	'id'         => $prefix . 'icon_features',
	'type'       => 'text',
	'repeatable' => true,
) );

$cmb_pricetable->add_field( array(
	'name'       => __( 'Add Items', seowave ),
	'id'         => $prefix . 'plan_features',
	'type'       => 'text',
	'repeatable' => true,
) );
*/

// $group_field_id is the field id string, so in this case: $prefix . 'demo'
$cmb_grp_pricetable = $cmb_pricetable->add_field( array(
	'id'          => $prefix . 'pricetable_grp',
	'type'        => 'group',
	'options'     => array(
		'group_title'   => __( 'Content Box {#}', 'seowave' ), // {#} gets replaced by row number
		'add_button'    => __( 'Add Item', 'seowave' ),
		'remove_button' => __( 'Remove Item', 'seowave' ),
	),
) );

$cmb_pricetable->add_group_field( $cmb_grp_pricetable, array(
	'name' => 'Icon',
	'id'   => 'pricetable_icon',
	'type' => 'text',
) );

$cmb_pricetable->add_group_field( $cmb_grp_pricetable, array(
	'name' => 'Title',
	'id'   => 'pricetable_title',
	'type' => 'text',  
) );

$cmb_pricetable->add_field( array(
	'name'         => __( 'Button Text ', 'seowave' ),
	'id'           => $prefix . 'btntxt',
	'type'         => 'text',		
) );

$cmb_pricetable->add_field( array(
	'name'         => __( 'Button URL ', 'seowave' ),
	'id'           => $prefix . 'btnurl',
	'type'         => 'text',		
) );

?>