<?php

// Start with an underscore to hide fields from custom fields list
$prefix = 'ow_cf_';

/* - Page Description */

$cmb_product = new_cmb2_box( array(
	'id'            => $prefix . 'metabox_product',
	'title'         => __( 'Product Options', "seowave" ),
	'object_types'  => array( 'product' ), // Post type
	'context'       => 'normal',
	'priority'      => 'high',
	'show_names'    => true, // Show field names on the left
) );

$cmb_product->add_field( array(
	'name'             => 'Price Per',
	'desc'             => 'Select an option',
	'id'               => $prefix . 'price_per',
	'type'             => 'select',
	'default'          => 'month',
	'options'          => array(
		'month' => __( 'Month', "seowave" ),
		'day'   => __( 'Day', "seowave" ),
		'year'   => __( 'Year', "seowave" ),
	),
) );
?>