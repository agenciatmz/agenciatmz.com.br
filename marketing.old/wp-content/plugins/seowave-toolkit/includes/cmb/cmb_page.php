<?php

// Start with an underscore to hide fields from custom fields list
$prefix = 'ow_cf_';

/* - Page Description */

$cmb_page = new_cmb2_box( array(
	'id'            => $prefix . 'metabox_page',
	'title'         => __( 'Page Options', "seowave" ),
	'object_types'  => array( 'page', 'post', 'ow_services', 'ow_casestudy' ), // Post type
	'context'       => 'normal',
	'priority'      => 'high',
	'show_names'    => true, // Show field names on the left
) );

$cmb_page->add_field( array(
	'name'             => 'Page Breadcrumb',
	'desc'             => 'Select an option',
	'id'               => $prefix . 'page_breadcrumb',
	'type'             => 'select',
	'default'          => 'enable',
	'options'          => array(
		'enable' => __( 'Enable', 'seowave' ),
		'disable'   => __( 'Disable', 'seowave' ),
	),
) );

$cmb_page->add_field( array(
	'name'             => 'Page Layout',
	'desc'             => 'Select an option',
	'id'               => $prefix . 'page_layout',
	'type'             => 'select',
	'default'          => 'fixed',
	'options'          => array(
		'fixed' => __( 'Fixed', "seowave" ),
		'fluid'   => __( 'Fluid', "seowave" ),
	),
) );

$cmb_page->add_field( array(
	'name'             => 'Sidebar Position',
	'desc'             => 'Select an option',
	'id'               => $prefix . 'sidebar_layout',
	'type'             => 'select',
	'default'          => 'primary_sidebar',
	'options'          => array(
		'right_sidebar'   => __( 'Right', "seowave" ),
		'left_sidebar' => __( 'Left', "seowave" ),
		'no_sidebar'   => __( 'None', "seowave" ),
	),
) );

$cmb_page->add_field( array(
	'name'             => 'Widget Area',
	'desc'             => 'Select an option',
	'id'               => $prefix . 'widget_area',
	'type'             => 'select',
	'default'          => 'sidebar-1',
	'options'          => array(
		'sidebar-1' => __( 'Primary Sidebar', "seowave" ),
		'sidebar-2'   => __( 'Secondary Sidebar', "seowave" ),
		'sidebar-3'   => __( 'Wocommerce Sidebar', "seowave" ),
		'sidebar-4'   => __( 'Services Sidebar', "seowave" ),
	),
) );

$cmb_page->add_field( array(
	'name'             => 'Page Header',
	'desc'             => 'Select an option',
	'id'               => $prefix . 'page_title',
	'type'             => 'select',
	'default'          => 'enable',
	'options'          => array(
		'enable' => __( 'Enable', "seowave" ),
		'disable'   => __( 'Disable', "seowave" ),
	),
) );

$cmb_page->add_field( array(
	'name'       => __( 'Sub Title', "seowave" ),
	'id'         => $prefix . 'page_sub_title',
	'type'       => 'text',
) );

$cmb_page->add_field( array(
	'name' => __( 'Header Image', "seowave" ),
	'desc' => __( 'Upload an image or enter a URL.', "seowave" ),
	'id'   => $prefix . 'page_header_img',
	'type' => 'file',
) );
?>