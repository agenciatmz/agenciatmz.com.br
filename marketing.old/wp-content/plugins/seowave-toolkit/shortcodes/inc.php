<?php
if( !function_exists('ow_sc_setup') ) :

	function ow_sc_setup() {

		/* Script For Shortcodes */
		add_image_size( 'seowave-271-320', 271, 320, true ); // CaseStudy
		add_image_size( 'seowave-917-534', 917, 534, true ); // CaseStudy Single
		add_image_size( 'seowave-579-460', 579, 460, true ); // CaseStudy Single
		add_image_size( 'seowave-550-164', 550, 164, true ); // CaseStudy Single left Counters Graph
		add_image_size( 'seowave-550-166', 550, 166, true ); // CaseStudy Single Right Counters Graph
		add_image_size( 'seowave-105-105', 105, 105, true ); // Service
		add_image_size( 'seowave-360-368', 360, 368, true ); // Service Single
		add_image_size( 'seowave-26-26', 26, 26, true ); // Service Single ICon Image
		add_image_size( 'seowave-651-264', 651, 264, true ); // Service Single Ranking Image
		add_image_size( 'seowave-290-355', 290, 355, true ); // Team
		add_image_size( 'seowave-78-78', 78, 78, true ); // Testimonials
		add_image_size( 'seowave-109-47', 109, 47, true ); // Clients
		add_image_size( 'seowave-549-396', 549, 396, true ); // About
		add_image_size( 'seowave-27-27', 27, 27, true ); // About Accordion
		add_image_size( 'seowave-370-266', 370, 266, true ); // Blog Section
		add_image_size( 'seowave-387-479', 387, 479, true ); // Our Apps
		add_image_size( 'seowave-64-64', 64, 64, true ); // Our Sucess Process
		add_image_size( 'seowave-403-439', 403, 439, true ); // Offer Service Crousel
		add_image_size( 'seowave-23-31', 23, 31, true ); // Offer Service Icon
	}

	add_action( 'after_setup_theme', 'ow_sc_setup' );
endif;

if( function_exists('vc_map') ) { 

	vc_add_param("vc_row", array(
		"type" => "dropdown",
		"group" => "Page Layout",
		"class" => "",
		"heading" => "Type",
		"param_name" => "type",
		'value' => array(
			__( 'Default', 'seowave' ) => 'default-layout',
			__( 'Fixed', 'seowave' ) => 'container',
		),
	));

	/* Include all individual shortcodes. */

	$prefix_sc = "sc_";

	require_once( $prefix_sc . "blog.php" );

	require_once( $prefix_sc . "calltoaction.php" );

	require_once( $prefix_sc . "casestudy.php" );

	require_once( $prefix_sc . "contact_form.php" );

	require_once( $prefix_sc . "map.php" );

	require_once( $prefix_sc . "ourapps.php" );

	require_once( $prefix_sc . "pricetable.php" );

	require_once( $prefix_sc . "services.php" );

	require_once( $prefix_sc . "tweet.php" );

	require_once( $prefix_sc . "about.php" );

	require_once( $prefix_sc . "carousel.php" );

	require_once( $prefix_sc . "clients.php" );

	require_once( $prefix_sc . "faq.php" );

	require_once( $prefix_sc . "feature.php" );

	require_once( $prefix_sc . "offerservice.php" );

	require_once( $prefix_sc . "ourprocess.php" );

	require_once( $prefix_sc . "statistics.php" );

	require_once( $prefix_sc . "team.php" );

	require_once( $prefix_sc . "testimonials.php" );

	require_once( $prefix_sc . "packages.php" );
}