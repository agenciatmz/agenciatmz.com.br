<?php
function ow_pricetable( $atts ) {

	extract( shortcode_atts(
		array(
			'title' => '',
			'desc' => '',
			'posts_display' => ''
		), $atts )
	);
	$post_type = 'ow_pricetable';
	
	$args = array(
		'post_type' => $post_type,
		'posts_per_page' => $posts_display,
		'order'   => 'ASC',
	);

	$qry = new WP_Query( $args );

	if( '' === $posts_display ) :
		$posts_display = 3;
	endif;

	ob_start();
	
	?>
	<section class="row">
		<div class="row m0 page_packages page_pricing contentRow">
			<div class="container">
				<div class="row title_row">
					<?php
						echo seowave_content('<h2>','</h2>', esc_attr( $title ) );
						echo seowave_content('<h5>','</h5>', esc_attr( $desc ) );
					?>
				</div>
				<div class="row pricingTab">
					<ul class="nav nav-tabs" role="tablist" id="pricingTab">
						<?php
							$cnt = 1;
							$pricetable_terms = get_terms("ow_pricetable_taxonomy");
							foreach( $pricetable_terms as $pricetable_term ) : ?>
								<li role="presentation" class="<?php if( $cnt == 1 ) { echo "active"; } ?>">
									<a href="#<?php echo $pricetable_term->slug ?>" data-toggle="tab"><?php echo $pricetable_term->name ?></a>
								</li>
							<?php
							$cnt++;
							endforeach;
						?>
					</ul>
					
					<div class="tab-content">
						<?php
							$cnt = 1;
							foreach( $pricetable_terms as $pricetable_term ) : 
							?>
							<div role="tabpanel" class="tab-pane <?php if( $cnt == 1 ) { echo "active"; } ?>" id="<?php echo $pricetable_term->slug ?>">
								<?php	
									$args = array(
										'post_type' => 'ow_pricetable', 
										'tax_query' => array(
											array(
											'taxonomy' => 'ow_pricetable_taxonomy',
											'field' => 'id',
											'terms' => $pricetable_term->term_id
											 )
										  ),	
										'posts_per_page' => -1,
										'order'   => 'ASC'
									);
									$qry = new WP_Query($args);
									while($qry->have_posts()) : $qry->the_post();	   							
										?>
										<div class="col-sm-4 pricing_table">
											<div class="row m0 inner">
												<?php the_title('<h4 class="pricing_title">','</h4>'); ?>
												<div class="row m0 pricing_price">
													<div class="row m0 round_box">
														<div class="price_inner">
															<span class="price"><?php _e('$','seowave'); ?><?php echo esc_attr( get_post_meta( get_the_ID(), 'ow_cf_price_amt', true ) );?></span><br>
															<?php echo esc_attr( get_post_meta( get_the_ID(), 'ow_cf_price_cost', true ) ); ?>
														</div>
													</div>
												</div>
												<ul class="list-unstyled feature row m0">
													<?php 
														$pricetable_grp_txt = get_post_meta( get_the_ID(), 'ow_cf_pricetable_grp', true );
														if( count( $pricetable_grp_txt ) > 0 && is_array( $pricetable_grp_txt ) ) {
															foreach ( (array) $pricetable_grp_txt as $key => $value ) {
																if ( isset( $value['pricetable_icon'] )|| isset( $value['pricetable_title'] ) ) {
																	?>
																	<li class="fa <?php echo esc_html( $value['pricetable_icon'] ); ?>"><?php echo esc_html( $value['pricetable_title'] ); ?></li>
																	<?php
																}
															}
														}
													?>
												</ul>
												<?php echo seowave_content('<a href="','" class="purchase_btn">'.esc_attr( get_post_meta( get_the_ID(), 'ow_cf_btntxt', true ) ).'</a>', esc_url( get_post_meta( get_the_ID(), 'ow_cf_btnurl', true ) ) ); ?>
											</div>
										</div>
										<?php
									endwhile;	
								?>
								</div>
								<?php	
								$cnt++;
							endforeach;	
							
							// Reset Post Data
							wp_reset_postdata();
						?>		
					</div>
				</div>
			</div>
		</div>
	</section>
	
	<?php
	return ob_get_clean();
}
add_shortcode('ow_pricetable', 'ow_pricetable');	

/* - Price Table */
vc_map( array(
	"name" => __("Price Table", 'seowave'),
	"icon" => 'vc-site-icon',
	"base" => "ow_pricetable",
	"category" => __('Seowave', 'seowave'),
	"params" => array(
		
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Title", 'seowave'),
			"param_name" => "title",
			"holder" => "div",
		),

		array(
			"type" => "textarea",
			"class" => "",
			"heading" => __("Short Description", 'seowave'),
			"param_name" => "desc",
			"holder" => "div",
		),
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Post Per Page Display", 'seowave'),
			"param_name" => "posts_display",
			"holder" => "div",
		),
	)
) );