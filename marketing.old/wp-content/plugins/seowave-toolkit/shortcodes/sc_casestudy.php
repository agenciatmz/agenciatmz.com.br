<?php
function ow_casestudy( $atts ) {

	extract( shortcode_atts(
		array(
		'title' => '',
		'desc'  => '',
		'per_page' => '',
		'owlayout' => ''
		), $atts )
	);

	if( '' === $per_page ) :
		$per_page = -1;
	endif;

	$post_type = 'ow_casestudy';
	$post_tax = 'ow_casestudy_tax';

	$qry_args = array(
		'post_type' => $post_type,
		'posts_per_page' => $per_page,
		'order' => 'ASC',
	);
	
	if( '' === $owlayout ) :
		$owlayout = "layout_one";
	endif;
	
	$qry = new WP_Query( $qry_args );

	ob_start();
	
	if( $owlayout == "layout_one" ) {
		?>
		<div class="row m0 recent_cases contentRow">
			<div class="container">
				<div class="row title_row">
					<?php 
						echo seowave_content('<h2>','</h2>',esc_attr( $title ) ); 
						echo seowave_content('<h5>','</h5>',esc_html( $desc ) ); 
					?>
				</div>
				<?php
					if( $qry->have_posts() ) {
						?>
						<div class="recentCaseStudies">
							<?php
								while ( $qry->have_posts() ) : $qry->the_post();
									?>
									<div class="case col-md-3 col-sm-6 ecommerce">
										<div class="m0"> 
											<a href="<?php echo esc_url( get_permalink() ); ?>" class="img_link">
												<?php the_post_thumbnail( 'seowave-271-320' ); ?>
											</a> 
											<a href="<?php echo esc_url( get_permalink() ); ?>" class="text_link">
												<?php _e('View details','seowave'); ?> 
												<i class="fa fa-angle-right"></i>
											</a>
										</div>
									</div>
									<?php
								endwhile;
								
							// Reset Post Data
							wp_reset_postdata();
							?>
						</div>
						<?php
					}
				?>
			</div>
			<div class="row dot270"></div>
		</div>
		<?php
	}
	elseif( $owlayout == "layout_two" ) {
		?>
		<section class="row page_case">
			<div class="container">
				<div class="row">
					<ul id="filters" class="list-inline">			
						<li class="active"  data-filter="*"><?php _e( 'All', 'seowave' ) ?></li>
							<?php
							$terms = get_terms( $post_tax );
							if ( count( $terms ) > 0 && is_array( $terms ) ) {
								foreach ( $terms as $term ) {
									?>
									<li data-filter=".<?php echo esc_attr ( $term->slug ); ?>"><?php echo esc_attr ( $term->name ); ?></li>
									<?php
								}
							}
						?>
					</ul>
				</div>
				<div class="" id="caseStdudies">
					<?php
						while ( $qry->have_posts() ) : $qry->the_post();
							$terms = get_the_terms( get_the_ID(), 'ow_casestudy_tax' );
							if ( $terms && ! is_wp_error( $terms ) ) :
								$links = array();
								foreach ( $terms as $term )
								{
									$links[] = $term->slug;
								}
								$tax_links = join( " ", str_replace(' ', '-', $links));
							endif;
							?>
							<div class="case case-portfolio col-md-3 col-sm-6 <?php echo esc_attr ( $tax_links ); ?>">
								<div class="m0">
									<a href="<?php echo esc_url( get_permalink() ); ?>" class="img_link">
										<?php the_post_thumbnail( 'seowave-271-320' ); ?>
									</a>
									
									<a href="<?php echo esc_url( get_permalink() ); ?>" class="text_link">
										<?php _e('View details','seowave'); ?><i class="fa fa-angle-right"></i>
									</a>
									
								</div>
							</div>
							
							<?php
						endwhile;
								
						// Reset Post Data  	
						wp_reset_postdata();
					?>
				</div>
			</div>
		</section>
		<?php
	}
	return ob_get_clean();
}
add_shortcode('ow_casestudy', 'ow_casestudy');

/* - Case Study */
vc_map( array(
	"name" => __("Case Study", 'seowave'),
	"icon" => 'vc-site-icon',
	"base" => "ow_casestudy",
	"category" => __('Seowave', 'seowave'),
	"params" => array(
		
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Title", 'seowave'),
			"param_name" => "title",
			"holder" => "div",
		),

		array(
			"type" => "textarea",
			"class" => "",
			"heading" => __("Short Description", 'seowave'),
			"param_name" => "desc",
			"holder" => "div",
		),
		
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Post Per Page", 'seowave'),
			"param_name" => "per_page",
			"holder" => "div",
		),
		
		array(
			'type' => 'dropdown',
			'class' => '',
			'heading' => __( 'Layout', 'seowave' ),
			'param_name' => 'owlayout',
			'value' =>array(
				'Layout 1' => 'layout_one',
				'Layout 2' => 'layout_two',
			),
			'description' => __( 'Select Post Layout. Default Layout 1 Select', 'seowave' ),
		),
	)
) );