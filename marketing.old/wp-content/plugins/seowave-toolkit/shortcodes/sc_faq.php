<?php
function sc_faq_outer( $atts, $content = null ) {

	extract( shortcode_atts( array(

		'title' => '',
		'desc' => '',

		'extra_class' => ''

	), $atts ) );

	$result = "
		<div class='page_packages page_faq contentRow'>                
			<div class='container'>
				<div class='row title_row'>
					<h2>$title</h2>
					<h5>$desc</h5>
				</div>
				<div class='faqs'>
					".do_shortcode( $content )."
				</div>
			</div>
		</div>";

	return $result;
}
add_shortcode( 'faq_outer', 'sc_faq_outer' );

function sc_faq_inner( $atts, $content = null ) {

	extract( shortcode_atts( array(

		'title' => '',
		'extra_class' => ''

	), $atts ) );
	
	$i = 1;
	
	$result = "
		<div class='col-sm-6 faq_clear'>
			<div class='faq'>
				<h5>$title</h5>
				<p>$content</p>
			</div>
		</div>";
	return $result;

}
add_shortcode( 'faq_inner', 'sc_faq_inner' );

// Parent Element
function vc_faq_outer() {

	// Register "container" content element. It will hold all your inner (child) content elements
	vc_map( array(
		"name" => __("Faq", "seowave"),
		"base" => "faq_outer",
		"category" => esc_html__('Seowave', 'seowave'),
		"as_parent" => array('only' => 'faq_inner'), // Use only|except attributes to limit child shortcodes (separate multiple values with comma)
		"content_element" => true,
		"show_settings_on_create" => true,
		"is_container" => true,
		"params" => array(
			// add params same as with any other content element
			array(
				"type" => "textfield",
				"heading" => __("Title", "seowave"),
				"param_name" => "title",
			),
			array(
				"type" => "textarea",
				"heading" => __("Short Description", "seowave"),
				"param_name" => "desc",
			),
			array(
				"type" => "textfield",
				"heading" => __("Extra class name", "seowave"),
				"param_name" => "el_class",
				"description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "seowave")
			)
		),
		"js_view" => 'VcColumnView'
	) );
}
add_action( 'vc_before_init', 'vc_faq_outer' );

// Nested Element
function vc_faq_inner() {

	vc_map( array(
		"name" => __("Single Faq", "seowave"),
		"base" => "faq_inner",
		"category" => esc_html__('Seowave', 'seowave'),
		"content_element" => true,
		"as_child" => array('only' => 'faq_outer'), // Use only|except attributes to limit parent (separate multiple values with comma)
		"params" => array(
			// add params same as with any other content element
			
			array(
				"type" => "textfield",
				"heading" => __("Title", "seowave"),
				"param_name" => "title",
				"holder" => "div",
			),
			
			array(
				"type" => "textarea",
				"class" => "",
				"heading" => __("Description", "seowave"),
				"param_name" => "content",
				"holder" => "div",
			),
	
			array(
				"type" => "textfield",
				"heading" => __("Extra class name", "seowave"),
				"param_name" => "el_class",
				"description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "seowave")
			)
		)
	) );
}
add_action( 'vc_before_init', 'vc_faq_inner' );

// Your "container" content element should extend WPBakeryShortCodesContainer class to inherit all required functionality
if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {

    class WPBakeryShortCode_Faq_Outer extends WPBakeryShortCodesContainer {
	}
}

if ( class_exists( 'WPBakeryShortCode' ) ) {

    class WPBakeryShortCode_Faq_Inner extends WPBakeryShortCode {
    }
}
?>