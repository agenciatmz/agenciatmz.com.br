<?php
function ow_ourapps( $atts, $content = null ) {

	extract( shortcode_atts(
		array(
			
			'title' => '',
			'desc' => '',
			'servicefirst' => '',
			'servicesecond' => '',
			'servicethired' => '',
			'app_image' => '',
			'apple_store' => '',
			'gplay_store' => '',
			'bg'  => ''
		), $atts )
	);
	
	if( seowave_checkstring( $bg ) ) {
		$style= 'background-image:url('.wp_get_attachment_url($bg).');';
	}
	else {
		$style = "";
	}
	
	ob_start();

	?>
	<section class="row marketing_app" style="<?php echo esc_attr($style); ?>">
		<div class="container">
			<div class="row">
				<div class="col-sm-7">
					<div class="row">
						<?php
							echo seowave_content('<h2 class="page_title white_c">','</h2>', esc_attr($title) );
							echo seowave_content('<p>','</p>', esc_html($desc) );
						?>
						<ul class="nav">
							<li>
								<img src="<?php echo esc_url( IMG_URI ); ?>/icons/app/1.png" alt="">
								<?php echo esc_attr( $servicefirst ); ?>
							</li>
							
							<li>
								<img src="<?php echo esc_url( IMG_URI ); ?>/icons/app/2.png" alt="">
								<?php echo esc_attr( $servicesecond ); ?>
							</li>
							
							<li>
								<img src="<?php echo esc_url( IMG_URI ); ?>/icons/app/3.png" alt="">
								<?php echo esc_attr( $servicethired ); ?>
							</li>
							
						</ul>
						<div class=" m0 download_links"> 
							<a href=<?php echo esc_url( $apple_store ) ?>>
								<img src="<?php echo esc_url( IMG_URI ); ?>/icons/app/4.png" alt="">
							</a>
							
							<a href="<?php echo esc_url( $gplay_store ) ?>">
								<img src="<?php echo esc_url( IMG_URI ); ?>/icons/app/5.png" alt="">
							</a>
							
						</div>
					</div>
				</div>
				<div class="col-sm-4 col-sm-offset-1">
					<div class="row"> 
						<?php
							if( seowave_checkstring( $app_image ) ) {
								echo wp_get_attachment_image( $app_image, 'seowave-387-479' );
							}
						?>
					</div>
				</div>
			</div>
		</div>
	</section>
	
	<?php
	return ob_get_clean();
}
add_shortcode('ow_ourapps', 'ow_ourapps');

/* - Our Applications */
vc_map( array(
	"name" => __("Our Applications", 'seowave'),
	"icon" => 'vc-site-icon',
	"base" => "ow_ourapps",
	"category" => __('Seowave', 'seowave'),
	"params" => array(
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Title", 'seowave'),
			"param_name" => "title",
			"holder" => "div",
		),
		array(
			"type" => "textarea",
			"class" => "",
			"heading" => __("Description", 'seowave'),
			"param_name" => "desc",
			"holder" => "div",
		),
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Support Title One", 'seowave'),
			"param_name" => "servicefirst",
			"holder" => "div",
		),
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Support Title Second", 'seowave'),
			"param_name" => "servicesecond",
			"holder" => "div",
		),
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Support Title Second", 'seowave'),
			"param_name" => "servicethired",
			"holder" => "div",
		),
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Apple Store Link", 'seowave'),
			"param_name" => "apple_store",
			"holder" => "div",
		),
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Googleplay Store Link", 'seowave'),
			"param_name" => "gplay_store",
			"holder" => "div",
		),
		array(
			"type" => "attach_image",
			"class" => "",
			"heading" => __("Image", 'seowave'),
			"param_name" => "app_image",
		),
		array(
			"type" => "attach_image",
			"class" => "",
			"heading" => __("Background Image", 'seowave'),
			"param_name" => "bg",
		),
	)
) );