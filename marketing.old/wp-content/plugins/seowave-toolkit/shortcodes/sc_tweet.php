<?php
function ow_tweet( $atts ) {

	extract( shortcode_atts(
		array(
			
			'bg' => '',
			'consumer_key' => '',
			'consumer_secret' => '',
			'access_token' => '',
			'access_token_secret' => '',
			'username' => '',
			
		), $atts )
	);
	
	if( seowave_checkstring( $bg ) ) {
		
		$style= 'background-image:url('.wp_get_attachment_url($bg).');';
	}
	else {
		$style = "";
	}

	require_once 'twitteroauth/twitteroauth.php';
	
	ob_start();
		
	$twitterConnection = new TwitterOAuth($consumer_key, $consumer_secret, $access_token, $access_token_secret );
	$twitterData = $twitterConnection->get('statuses/user_timeline',array('screen_name' => $username, 'count' => 5,'exclude_replies' => 1));

	$transName = 'list_tweets';
	if( $twitterConnection->http_code != 200 ) {
		 $twitterData = get_transient( $transName );
	 }
	set_transient($transName, $twitterData, 60 * 10);
	$twitter = get_transient($transName);
	?>
	<section class="tweets"  style="<?php echo esc_attr( $style )?>">
		<div class="container">
			<div class="icon">
				<i class="fa fa-twitter"></i>
			</div>
			<div class="flexslider tweet_slider">
				<ul class="slides">	
					<?php
					if( $twitter && is_array( $twitter ) ) {

						foreach( $twitter as $tweet ) {

							$latestTweet = $tweet->text;
							$latestTweet = preg_replace('/http:\/\/([a-z0-9_\.\-\+\&\!\#\~\/\,]+)/i', '<a href="http://$1" target="_blank">http://$1</a>', $latestTweet);
							$latestTweet = preg_replace('/@([a-z0-9_]+)/i', '<a href="http://twitter.com/$1" target="_blank">@$1</a>', $latestTweet);
							
							$twitterTime = strtotime($tweet->created_at);
							$twitterTime = !empty($tweet->utc_offset) ? $twitterTime+($tweet->utc_offset ) : $twitterTime;
							$timeAgo = date_i18n(  get_option('date_format'), $twitterTime ); 
							?>
							<li>
								<p class="tweet">
									<?php echo html_entity_decode( $latestTweet ); ?>
								</p>
								<p class="date">
									<?php echo html_entity_decode( $timeAgo ); ?>
								</p>
							</li>
							<?php
						}
					}
					?>
				</ul>
			</div>
		</div>
	</section>
	
	<?php
	return ob_get_clean();
}
add_shortcode('ow_tweet', 'ow_tweet');

/* - Recent Tweets */
vc_map( array(
	"name" => __("Recent Tweets", 'seowave'),
	"icon" => 'vc-site-icon',
	"base" => "ow_tweet",
	"category" => __('Seowave', 'seowave'),
	"params" => array(
		
		array(
			"type" => "attach_image",
			"class" => "",
			"heading" => __("Background Image", 'seowave'),
			"param_name" => "bg",
			"holder" => "div",
		),
		
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("consumer key", 'seowave'),
			"param_name" => "consumer_key",
			"holder" => "div",
		),
		
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("consumer secret", 'seowave'),
			"param_name" => "consumer_secret",
			"holder" => "div",
		),
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("access token", 'seowave'),
			"param_name" => "access_token",
			"holder" => "div",
		),
		
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("access token secret", 'seowave'),
			"param_name" => "access_token_secret",
			"holder" => "div",
		),
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("username", 'seowave'),
			"param_name" => "username",
			"holder" => "div",
		),
	)
) );