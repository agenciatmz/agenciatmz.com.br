<?php
function ow_contact_map( $atts ) {
	
	extract( shortcode_atts(
		array(
			'wtime' => '',
			'wholiday' => '',
			'addressone' => '',
			'addresstwo' => '',
			'phone' => '',
			'email' => '',
			'infotxt' => '',
			'btntxt' => '',
			'btnurl' => '',
			'vc_map_lati' => '',
			'vc_map_longi' => '',
			'vc_address' => '',
		), $atts )
	);

	ob_start();

	?>
	
	<div class="row contact_details m0 text-center contact-address">
		<div class="col-sm-3 contact_widgets contact_details_inner">
			<div class="row icon_row">
				<img src="<?php echo esc_url( IMG_URI ); ?>/contact/1.png" alt="">
			</div>
			<h4><?php _e('Working hours','seowave'); ?></h4>
			<p><?php _e('(Mon to Sat): ','seowave'); ?> <?php echo esc_attr( $wtime ); ?> <span><?php _e('(Sunday): ','seowave'); ?><?php echo esc_attr( $wholiday ); ?></span></p>
		</div>
		
		<div class="col-sm-3 contact_widgets contact_details_inner">
			<div class="row icon_row">
				<img src="<?php echo esc_url( IMG_URI ); ?>/contact/2.png" alt="">
			</div>
			<h4><?php _e('Address','seowave'); ?></h4>
			<p><?php echo esc_attr( $addressone ); ?><span><?php echo esc_attr( $addresstwo ); ?></span></p>
		</div>
		<div class="col-sm-3 contact_widgets">
			<div class="row icon_row"><img src="<?php echo esc_url( IMG_URI ); ?>/contact/3.png" alt=""></div>
			<h4><?php _e('Contact Details','seowave'); ?></h4>
			<p><span><?php _e('Phone : ','seowave'); ?></span><?php echo esc_attr( $phone ); ?> </p> 
			<p><span><?php _e('Support: ','seowave'); ?></span><?php echo esc_attr( $email ); ?></p>
		</div>
		<div class="col-sm-3 contact_widgets">
			<div class="row icon_row"><img src="<?php echo esc_url( IMG_URI ); ?>/contact/4.png" alt=""></div>
			<h4><?php _e('Careers ','seowave'); ?></h4>
			<p><?php echo esc_attr( $infotxt ); ?></p>
			<a href="<?php echo esc_url( $btnurl ); ?>" class="borderred_link"><span><?php echo esc_attr( $btntxt ); ?></span></a>
		</div>
	</div>
	
    <div class="map">
		<div class="map-canvas" id="map-canvas-contact" data-lat="<?php echo esc_attr( $vc_map_lati ); ?>" data-lng="<?php echo esc_attr( $vc_map_longi ); ?>" data-string="<?php echo esc_attr( $vc_address ); ?>" data-marker="<?php echo esc_url( OWTH_LIB ).'images/map-marker.png'; ?>" data-zoom="12"></div>
	</div>
	
	<?php
	return ob_get_clean();
}
add_shortcode('ow_contact_map', 'ow_contact_map');

/* - Contact Map */
vc_map( array(
	"name" => __("Contact Map", 'seowave'),
	"icon" => 'vc-site-icon',
	"base" => "ow_contact_map",
	"category" => __('Seowave', 'seowave'),
	"params" => array(
		
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Working Time", 'seowave'),
			"param_name" => "wtime",
			"description" => __("e.g : 10.00 AM to 07.00 PM", 'seowave'),
		),
		
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Holiday", 'seowave'),
			"param_name" => "wholiday",
			"description" => __("e.g : Holiday", 'seowave'),
		),
		
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Address Line 1", 'seowave'),
			"param_name" => "addressone",
			"description" => __("e.g : 14 New Digital Street, 25 North Corne,", 'seowave'),
			"holder" => "div",
		),
		
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Address Line 2", 'seowave'),
			"param_name" => "addresstwo",
			"description" => __("e.g : Sydney, Australia-1", 'seowave'),
			"holder" => "div",
		),
		
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Contact No", 'seowave'),
			"param_name" => "phone",
			"description" => __("e.g : +(61) 123 456 7890 ", 'seowave'),
		),
		
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Contact Email", 'seowave'),
			"param_name" => "email",
			"description" => __("e.g : info@example.com ", 'seowave'),
		),
		
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Information Text", 'seowave'),
			"param_name" => "infotxt",
			"description" => __("e.g : We Givbe you Opportunity to work join with us", 'seowave'),
		),
		
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Button Text", 'seowave'),
			"param_name" => "btntxt",
		),
		
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Button URL", 'seowave'),
			"param_name" => "btnurl",
		),
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Map Latitude", 'seowave'),
			"param_name" => "vc_map_lati",
			"description" => __("e.g : 40.5274081", 'seowave'),
		),
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Map Longitude", 'seowave'),
			"param_name" => "vc_map_longi",
			"description" => __("e.g : -105.0321923", 'seowave'),
		),
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Map Address", 'seowave'),
			"param_name" => "vc_address",
		),
	)
) );