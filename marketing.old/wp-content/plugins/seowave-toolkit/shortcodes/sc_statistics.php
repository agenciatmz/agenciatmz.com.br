<?php
function sc_statistics_outer( $atts, $content = null ) {

	extract( shortcode_atts( array(
		'bg' => '',
		'extra_class' => ''

	), $atts ) );

	if( seowave_checkstring( $bg ) ) {
		
		$style= 'background-image:url('.wp_get_attachment_url($bg).');';
	}
	else {
		$style = "";
	}
	
	$result = "
		<div class='row m0 funfacts_ab $extra_class' style='".($style)."' >
			<div class='container'>
				<div class='row'>
					".do_shortcode( $content )."
				</div>
			</div>
		</div>";

	return $result;
}
add_shortcode( 'statistics_outer', 'sc_statistics_outer' );

function sc_statistics_inner( $atts, $content = null ) {

	extract( shortcode_atts( array(
	
		'title' => '',
		'value' => '',
		'sign' => '',
		
		'extra_class' => ''

	), $atts ) );
	
	$result = "
		<div class='col-sm-3 fact statistics-content $extra_class'>
			<div class='counter-box'>
				<h3><span class='counter'>$value</span>$sign</h3>
				<h5>$title</h5>
			</div>
		</div>";

	return $result;
}
add_shortcode( 'statistics_inner', 'sc_statistics_inner' );

// Parent Element
function vc_statistics_outer() {

	// Register "container" content element. It will hold all your inner (child) content elements
	vc_map( array(
		"name" => __("Statistics", "seowave"),
		"base" => "statistics_outer",
		"category" => esc_html__('Seowave', 'seowave'),
		"as_parent" => array('only' => 'statistics_inner'), // Use only|except attributes to limit child shortcodes (separate multiple values with comma)
		"content_element" => true,
		"show_settings_on_create" => true,
		"is_container" => true,
		"params" => array(
			// add params same as with any other content element
			
			array(
				"type" => "attach_image",
				"heading" => __("Background Image", "seowave"),
				"param_name" => "bg",
			),
		),
		"js_view" => 'VcColumnView'
	) );
}
add_action( 'vc_before_init', 'vc_statistics_outer' );

// Nested Element
function vc_statistics_inner() {

	vc_map( array(
		"name" => __("Single Statistics", "seowave"),
		"base" => "statistics_inner",
		"category" => esc_html__('Seowave', 'seowave'),
		"content_element" => true,
		"as_child" => array('only' => 'statistics_outer'), // Use only|except attributes to limit parent (separate multiple values with comma)
		"params" => array(
			// add params same as with any other content element
			array(
				"type" => "textfield",
				"heading" => __("Title", "seowave"),
				"param_name" => "title",
				"holder" => "div",
			),
			array(
				"type" => "textfield",
				"heading" => __("Value", "seowave"),
				"param_name" => "value",
				"holder" => "div",
			),
			array(
				"type" => "textfield",
				"heading" => __("Symbol", "seowave"),
				"param_name" => "sign",
				"holder" => "div",
			),
			
			array(
				"type" => "textfield",
				"heading" => __("Extra class name", "seowave"),
				"param_name" => "el_class",
				"description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "seowave")
			)
		)
	) );
}
add_action( 'vc_before_init', 'vc_statistics_inner' );

// Your "container" content element should extend WPBakeryShortCodesContainer class to inherit all required functionality
if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {

    class WPBakeryShortCode_Statistics_Outer extends WPBakeryShortCodesContainer {
	}
}

if ( class_exists( 'WPBakeryShortCode' ) ) {

    class WPBakeryShortCode_Statistics_Inner extends WPBakeryShortCode {
    }
}
?>