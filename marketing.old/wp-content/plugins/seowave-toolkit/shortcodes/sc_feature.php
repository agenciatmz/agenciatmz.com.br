<?php
function sc_feature_outer( $atts, $content = null ) {

	extract( shortcode_atts( array(

		'extra_class' => ''

	), $atts ) );

	$result = "
		<section class='contentRow feature-section $extra_class'>
			<div class='container'>
				<div class='feature-inner'>
					".do_shortcode( $content )."
				</div>
			</div>
		</section>";

	return $result;
}
add_shortcode( 'feature_outer', 'sc_feature_outer' );

function sc_feature_inner( $atts, $content = null ) {

	extract( shortcode_atts( array(

		'feature_image' => '',
		'imglink' => '',
		'title' => '',
		'desc' => '',
		'btntxt' => '',
		'btnurl' => '',
		
		'extra_class' => ''

	), $atts ) );

	$feature_img = wp_get_attachment_image( $feature_image, 'seowave_290_355' );

	$result = "
		<div class='col-sm-4 feature_cols $extra_class'>
			<div class='media feature_col'>
				<div class='media-left'>
					<a href='".esc_url( $imglink )."'>
						$feature_img
					</a>
				</div>
				<div class='media-body'>
					<h5>$title</h5>
					<p>$desc</p>
					<a href='".esc_url( $btnurl )."' class='learn_more'>$btntxt</a> 
				</div>
			</div>
		</div>";

	return $result;
}
add_shortcode( 'feature_inner', 'sc_feature_inner' );

// Parent Element
function vc_feature_outer() {

	// Register "container" content element. It will hold all your inner (child) content elements
	vc_map( array(
		"name" => __("Feature", "seowave"),
		"base" => "feature_outer",
		"category" => esc_html__('Seowave', 'seowave'),
		"as_parent" => array('only' => 'feature_inner'), // Use only|except attributes to limit child shortcodes (separate multiple values with comma)
		"content_element" => true,
		"show_settings_on_create" => true,
		"is_container" => true,
		"params" => array(
			// add params same as with any other content element
			array(
				"type" => "label",
				"class" => "",
				"heading" => esc_html__("No need of settings", "social"),
				"param_name" => "label_partners",
			),
		),
		"js_view" => 'VcColumnView'
	) );
}
add_action( 'vc_before_init', 'vc_feature_outer' );

// Nested Element
function vc_feature_inner() {

	vc_map( array(
		"name" => __("Single Feature", "seowave"),
		"base" => "feature_inner",
		"category" => esc_html__('Seowave', 'seowave'),
		"content_element" => true,
		"as_child" => array('only' => 'feature_outer'), // Use only|except attributes to limit parent (separate multiple values with comma)
		"params" => array(
			// add params same as with any other content element
			array(
				"type" => "attach_image",
				"heading" => __("Feature Image", "seowave"),
				"param_name" => "feature_image",
			),
			array(
				"type" => "textfield",
				"heading" => __("Image Link", "seowave"),
				"param_name" => "imglink",
				"holder" => "div",
			),
			array(
				"type" => "textfield",
				"heading" => __("Title", "seowave"),
				"param_name" => "title",
				"holder" => "div",
			),
			array(
				"type" => "textarea",
				"heading" => __("Description", "seowave"),
				"param_name" => "desc",
				"holder" => "div",
			),
			array(
				"type" => "textfield",
				"heading" => __("Button Text", "seowave"),
				"param_name" => "btntxt",
				"holder" => "div",
			),
			array(
				"type" => "textfield",
				"heading" => __("Button URL", "seowave"),
				"param_name" => "btnurl",
				"holder" => "div",
			),
			array(
				"type" => "textfield",
				"heading" => __("Extra class name", "seowave"),
				"param_name" => "el_class",
				"description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "seowave")
			)
		)
	) );
}
add_action( 'vc_before_init', 'vc_feature_inner' );

// Your "container" content element should extend WPBakeryShortCodesContainer class to inherit all required functionality
if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {

    class WPBakeryShortCode_Feature_Outer extends WPBakeryShortCodesContainer {
	}
}

if ( class_exists( 'WPBakeryShortCode' ) ) {

    class WPBakeryShortCode_Feature_Inner extends WPBakeryShortCode {
    }
}
?>