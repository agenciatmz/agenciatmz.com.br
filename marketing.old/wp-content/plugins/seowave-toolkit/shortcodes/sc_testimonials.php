<?php
function sc_testimonials_outer( $atts, $content = null ) {

	extract( shortcode_atts( array(

		'title' => '',
		'desc' => '',
		'extra_class' => ''

	), $atts ) );

	$result = "
		<div class='testimonials contentRow  $extra_class'>
		<div class='container'>
			<div class='row'>
				<div class='row m0 title_row'>
					<h2>$title</h2>
					<h5>$desc</h5>
				</div>
				<div class='m0'>
					".do_shortcode( $content )."
				</div>
			</div>
		</div>
	</div>";

	return $result;
}
add_shortcode( 'testimonials_outer', 'sc_testimonials_outer' );

function sc_testimonials_inner( $atts, $content = null ) {

	extract( shortcode_atts( array(

		'testimonials_image' => '',
		'imglink' => '',
		'desc' => '',
		'title' => '',
		'designation' => '',
		'extra_class' => ''

	), $atts ) );

	$testimonials_img = wp_get_attachment_image( $testimonials_image, 'seowave_78_78' );

	$result = "
		<div class='col-sm-6 testimonial  $extra_class'>
		<div class='media'>
			<div class='media-left'>
				<a href='".esc_url( $imglink )."'>
					$testimonials_img
				</a>
			</div>
			<div class='media-body'>
				<div class='inner m0'>
					<div class='blockquote'>
						$desc
					</div>
					<div class='m0 identity'>
						$title
						<span class='pos'>($designation)</span>
					</div>
				</div>
			</div>
		</div>
	</div>";

	return $result;
}
add_shortcode( 'testimonials_inner', 'sc_testimonials_inner' );

// Parent Element
function vc_testimonials_outer() {

	// Register "container" content element. It will hold all your inner (child) content elements
	vc_map( array(
		"name" => __("Testimonials", "seowave"),
		"base" => "testimonials_outer",
		"category" => esc_html__('Seowave', 'seowave'),
		"as_parent" => array('only' => 'testimonials_inner'), // Use only|except attributes to limit child shortcodes (separate multiple values with comma)
		"content_element" => true,
		"show_settings_on_create" => true,
		"is_container" => true,
		"params" => array(
			// add params same as with any other content element
			array(
				"type" => "textfield",
				"heading" => __("Title", "seowave"),
				"param_name" => "title",
			),
			array(
				"type" => "textarea",
				"heading" => __("Description", "seowave"),
				"param_name" => "desc",
			),
			array(
				"type" => "textfield",
				"heading" => __("Extra class name", "seowave"),
				"param_name" => "el_class",
				"description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "seowave")
			)
		),
		"js_view" => 'VcColumnView'
	) );
}
add_action( 'vc_before_init', 'vc_testimonials_outer' );

// Nested Element
function vc_testimonials_inner() {

	vc_map( array(
		"name" => __("Single Testimonials", "seowave"),
		"base" => "testimonials_inner",
		"category" => esc_html__('Seowave', 'seowave'),
		"content_element" => true,
		"as_child" => array('only' => 'testimonials_outer'), // Use only|except attributes to limit parent (separate multiple values with comma)
		"params" => array(
			// add params same as with any other content element
			array(
				"type" => "attach_image",
				"heading" => __("Testimonials Image", "seowave"),
				"param_name" => "testimonials_image",
			),
			array(
				"type" => "textfield",
				"heading" => __("Image Link", "seowave"),
				"param_name" => "imglink",
			),
			array(
				"type" => "textarea",
				"heading" => __("Description", "seowave"),
				"param_name" => "desc",
			),
			
			array(
				"type" => "textfield",
				"heading" => __("Author", "seowave"),
				"param_name" => "title",
				"holder" => "div",
			),
			array(
				"type" => "textfield",
				"heading" => __("Designation", "seowave"),
				"param_name" => "designation",
				"holder" => "div",
			),
			array(
				"type" => "textfield",
				"heading" => __("Extra class name", "seowave"),
				"param_name" => "el_class",
				"description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "seowave")
			)
		)
	) );
}
add_action( 'vc_before_init', 'vc_testimonials_inner' );

// Your "container" content element should extend WPBakeryShortCodesContainer class to inherit all required functionality
if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {

    class WPBakeryShortCode_Testimonials_Outer extends WPBakeryShortCodesContainer {
	}
}

if ( class_exists( 'WPBakeryShortCode' ) ) {

    class WPBakeryShortCode_Testimonials_Inner extends WPBakeryShortCode {
    }
}
?>