<?php
function ow_calltoaction( $atts, $content = null ) {

	extract( shortcode_atts(
		array(
			'title' => '',
			'desc' => '',
			'btntxt' => '',
			'btnurl' => '',
			'owlayout' => '',
			'bg'  => ''
		), $atts )
	);
	
	if( '' === $owlayout ) :
		$owlayout = "layout_one";
	endif;
	
	if( seowave_checkstring( $bg ) ) {
		$style= 'background-image:url('.wp_get_attachment_url($bg).');';
	}
	else {
		$style = "";
	}
	
	ob_start();
	
	if( $owlayout == "layout_one" ) {
		?>
		<section class="row ready_to_work" style="<?php echo esc_attr($style); ?>">
			<div class="container">
				<div class="row">
				<?php 
					echo seowave_content('<h2>','</h2>',esc_attr($title) ); 
					echo seowave_content('<a href="','" class="borderred_link"><span>'.esc_attr( $btntxt ).'</span></a>', esc_url($btnurl) );
				?>
				</div>
			</div>
		</section>
		<?php
	}
	elseif( $owlayout == "layout_two" ) {
		?>
		<section class="trial30_banner" style="<?php echo esc_attr($style); ?>">
			<div class="container">
				<div class="row">
					<div class="col-sm-10">
						<?php
							echo seowave_content('<h2>','</h2>',esc_attr($title) ); 
							echo seowave_content('<h5>','</h5>',esc_attr($desc) ); 
						?>
					</div>
					<?php echo seowave_content('<div class="col-sm-2 button-text-link"><a href="','" class="borderred_link"><span>'.esc_attr( $btntxt ).'</span></a></div>', esc_url($btnurl) ); ?>
				</div>
			</div>
		</section>
		<?php
	}
	return ob_get_clean();
}
add_shortcode('ow_calltoaction', 'ow_calltoaction');

/* - Call to Action Blocks */
vc_map( array(
	"name" => __("Call to Action Blocks", 'seowave'),
	"icon" => 'vc-site-icon',
	"base" => "ow_calltoaction",
	"category" => __('Seowave', 'seowave'),
	"params" => array(
		array(
			"type" => "attach_image",
			"class" => "",
			"heading" => __("Backgroud Image", 'seowave'),
			"param_name" => "bg",
		),
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Title", 'seowave'),
			"param_name" => "title",
			"holder" => "div",
		),
		array(
			"type" => "textarea",
			"class" => "",
			"heading" => __("Description", 'seowave'),
			"param_name" => "desc",
		),
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Button Text", 'seowave'),
			"param_name" => "btntxt",
			"holder" => "div",
		),
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Button URL", 'seowave'),
			"param_name" => "btnurl",
			"holder" => "div",
		),
		array(
			'type' => 'dropdown',
			'class' => '',
			'heading' => __( 'Layout', 'seowave' ),
			'param_name' => 'owlayout',
			'value' =>array(
				'Layout 1' => 'layout_one',
				'Layout 2' => 'layout_two',
			),
			'description' => __( 'Select Post Layout. Default Layout 1 Select', 'seowave' ),
		),
	)
) );