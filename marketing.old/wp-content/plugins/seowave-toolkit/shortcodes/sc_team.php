<?php
function sc_team_outer( $atts, $content = null ) {

	extract( shortcode_atts( array(

		'title' => '',
		'desc' => '',

		'extra_class' => ''

	), $atts ) );

	$result = "
		<div class='row m0 team_members contentRow $extra_class'>
			<div class='container'>
				<div class='row'>                    
					<div class='row m0 title_row'>
						<h2>$title</h2>
						<h5>$desc</h5>
					</div>
					<div class='row m0'>
						".do_shortcode( $content )."
					</div>
				</div>
			</div>
	</div>";

	return $result;
}
add_shortcode( 'team_outer', 'sc_team_outer' );

function sc_team_inner( $atts, $content = null ) {

	extract( shortcode_atts( array(

		'team_image' => '',
		'title' => '',
		'designation' => '',
		'fb_url' => '',
		'twitter_url' => '',
		'linkedin_url' => '',
		'instagram_url' => '',
		
		'extra_class' => ''

	), $atts ) );

	$team_img = wp_get_attachment_image( $team_image, 'seowave_290_355' );

	$result = "
		<div class='col-sm-4 team $extra_class'>
			<div class='inner row'>
				<div class='m0 img'>
					$team_img
				</div>
				<ul class='nav social_lis'>
					<li><a href='".esc_url( $fb_url )."'><i class='fa fa-facebook'></i></a></li>						
					<li><a href='".esc_url( $twitter_url )."'><i class='fa fa-twitter'></i></a></li>
					<li><a href='".esc_url( $linkedin_url )."'><i class='fa fa-linkedin'></i></a></li>
					<li><a href='".esc_url( $instagram_url )."'><i class='fa fa-instagram'></i></a></li>
				</ul>
				<div class='identity m0'>
					<h5 class='name'>$title</h5>
					<h5 class='pos'>$designation</h5>
				</div>
			</div>
		</div>";

	return $result;
}
add_shortcode( 'team_inner', 'sc_team_inner' );

// Parent Element
function vc_team_outer() {

	// Register "container" content element. It will hold all your inner (child) content elements
	vc_map( array(
		"name" => __("Team", "seowave"),
		"base" => "team_outer",
		"category" => esc_html__('Seowave', 'seowave'),
		"as_parent" => array('only' => 'team_inner'), // Use only|except attributes to limit child shortcodes (separate multiple values with comma)
		"content_element" => true,
		"show_settings_on_create" => true,
		"is_container" => true,
		"params" => array(
			// add params same as with any other content element
			array(
				"type" => "textfield",
				"heading" => __("Title", "seowave"),
				"param_name" => "title",
			),
			array(
				"type" => "textarea",
				"heading" => __("Description", "seowave"),
				"param_name" => "desc",
			),
			array(
				"type" => "textfield",
				"heading" => __("Extra class name", "seowave"),
				"param_name" => "el_class",
				"description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "seowave")
			)
		),
		"js_view" => 'VcColumnView'
	) );
}
add_action( 'vc_before_init', 'vc_team_outer' );

// Nested Element
function vc_team_inner() {

	vc_map( array(
		"name" => __("Single Team", "seowave"),
		"base" => "team_inner",
		"category" => esc_html__('Seowave', 'seowave'),
		"content_element" => true,
		"as_child" => array('only' => 'team_outer'), // Use only|except attributes to limit parent (separate multiple values with comma)
		"params" => array(
			// add params same as with any other content element
			array(
				"type" => "attach_image",
				"heading" => __("Team Image", "seowave"),
				"param_name" => "team_image",
			),
			array(
				"type" => "textfield",
				"heading" => __("Title", "seowave"),
				"param_name" => "title",
				"holder" => "div",
			),
			array(
				"type" => "textfield",
				"heading" => __("Designation", "seowave"),
				"param_name" => "designation",
				"holder" => "div",
			),
			array(
				"type" => "textfield",
				"heading" => __("Facebook URL", "seowave"),
				"param_name" => "fb_url",
			),
			array(
				"type" => "textfield",
				"heading" => __("Twitter URL", "seowave"),
				"param_name" => "twitter_url",
			),
			array(
				"type" => "textfield",
				"heading" => __("Linkedin URL", "seowave"),
				"param_name" => "linkedin_url",
			),
			array(
				"type" => "textfield",
				"heading" => __("Instagram URL", "seowave"),
				"param_name" => "instagram_url",
			),
			array(
				"type" => "textfield",
				"heading" => __("Extra class name", "seowave"),
				"param_name" => "el_class",
				"description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "seowave")
			)
		)
	) );
}
add_action( 'vc_before_init', 'vc_team_inner' );

// Your "container" content element should extend WPBakeryShortCodesContainer class to inherit all required functionality
if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {

    class WPBakeryShortCode_Team_Outer extends WPBakeryShortCodesContainer {
	}
}

if ( class_exists( 'WPBakeryShortCode' ) ) {

    class WPBakeryShortCode_Team_Inner extends WPBakeryShortCode {
    }
}
?>