<?php

/* Define Constants */
define( 'THEME_OPTIONS', get_template_directory_uri() . '/admin/theme-options' );
define( 'ADMIN_URI', get_template_directory_uri() . '/admin' );
define( 'THEME_URI', get_template_directory_uri() );
define( 'IMG_URI', get_template_directory_uri() . '/images' );

/**
 * Register three widget areas.
 *
 * @since Seowave 1.0
 */
if ( ! function_exists( 'ow_widgets_init' ) ) {
	function ow_widgets_init() {
		register_sidebar( array(
			'name'          => __( 'Right Sidebar', 'seowave' ),
			'id'            => 'sidebar-1',
			'description'   => __( 'Appears in the Content section of the site.', 'seowave' ),
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h4 class="widget-title">',
			'after_title'   => '</h4>',
		));
		register_sidebar( array(
			'name'          => __( 'Left Sidebar', 'seowave' ),
			'id'            => 'sidebar-2',
			'description'   => __( 'Appears in the Content section of the site.', 'seowave' ),
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h4 class="widget-title">',
			'after_title'   => '</h4>',
		));
		register_sidebar( array(
			'name'          => __( 'Woocommerce Sidebar', 'seowave' ),
			'id'            => 'sidebar-3',
			'description'   => __( 'Appears in the Content section of the site.', 'seowave' ),
			'before_widget' => '<aside id="%1$s" class="widget woocommerce-widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h4 class="widget-title">',
			'after_title'   => '</h4>',
		));		
		register_sidebar( array(
			'name'          => __( 'Services Sidebar', 'seowave' ),
			'id'            => 'sidebar-4',
			'description'   => __( 'Appears in the Content section of the site.', 'seowave' ),
			'before_widget' => '<aside id="%1$s" class="widget woocommerce-widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h4 class="widget-title">',
			'after_title'   => '</h4>',
		));		
		register_sidebar( array(
			'name'          => __( 'Footer Widget Area 1', 'seowave' ),
			'id'            => 'sidebar-5',
			'description'   => __( 'Appears in the Content section of the site.', 'seowave' ),
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h4>',
			'after_title'   => '</h4><hr>',
		));
		register_sidebar( array(
			'name'          => __( 'Footer Widget Area 2', 'seowave' ),
			'id'            => 'sidebar-6',
			'description'   => __( 'Appears in the Content section of the site.', 'seowave' ),
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h4>',
			'after_title'   => '</h4><hr>',
		));
		register_sidebar( array(
			'name'          => __( 'Footer Widget Area 3', 'seowave' ),
			'id'            => 'sidebar-7',
			'description'   => __( 'Appears in the Content section of the site.', 'seowave' ),
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h4>',
			'after_title'   => '</h4><hr>',
		));
	}
	add_action( 'widgets_init', 'ow_widgets_init' );
}

/* Custom Excerpt Limit */
if ( ! function_exists( 'custom_excerpts' ) ) :
	function custom_excerpts( $limit ) {
		$excerpt = explode(' ', get_the_excerpt(), $limit);
		if ( count($excerpt) >= $limit ) :
		
			array_pop($excerpt);
			$excerpt = implode(" ",$excerpt).'...';
		
		else :
		
			$excerpt = implode(" ",$excerpt);
		
		endif; 

		$excerpt = preg_replace('`\[[^\]]*\]`','',$excerpt);
		return $excerpt;
	}
endif;

/* Check string for Null or Empty & Print It */
if ( ! function_exists( 'seowave_content' ) ) :

	function seowave_content( $before_val, $after_val, $val ) {

		if( $val != "" ) {
			return $before_val.$val.$after_val;
		}
		else {
			return "";
		}
	}
endif;

/* Check array element for Null or Empty */
if ( ! function_exists( 'seowave_checkarray' ) ) :
	function seowave_checkarray( $arrOptions, $strKey ) {

		if( is_array( $arrOptions )
			&& array_key_exists( $strKey, $arrOptions ) 
			&& isset( $arrOptions[$strKey] ) 
			//&& trim( $arrOptions[$strKey] ) != '' 
		) {
			return true;
		} 
		
		return false;
	}
endif;

/* Check string for Null or Empty */
if ( ! function_exists( 'seowave_checkstring' ) ) :
	function seowave_checkstring( $strValue ) {
		if ( isset( $strValue ) && trim( $strValue ) != '' ) :
			return true;
		endif;
		
		return false;
	}
endif;
?>