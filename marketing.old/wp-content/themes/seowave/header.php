<?php
/**
 * The Header for our theme
 *
 *
 * @package WordPress
 * @subpackage Seowave
 * @since Seowave 1.0
 */
 global $ow_option;
?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">

<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
	<header class="row header-main container-fluid no-padding">
		<div class="top_row container-fluid no-padding">
			<div class="container">
				<?php echo seowave_content('<div class="col-sm-6 col-xs-12 tagLine"><i class="fa fa-smile-o"></i>','</div>', esc_attr( $ow_option['opt_header_left'] ) ); ?>
				<div class="col-sm-6 col-xs-12">
					<ul class="nav navbar-right">
						<li>
							<?php
								if( class_exists( 'Woocommerce' ) ) {
									if ( is_user_logged_in() ) {
										?>
										<img alt="" src="<?php echo esc_url( IMG_URI ); ?>/icons/top-bar/user.png"/><a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" title="<?php _e('My Account', 'seowave'); ?>"><?php _e('My Account', 'seowave'); ?></a>
										<?php
									}
									else {
										?>
										<a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" title="<?php _e('Login / Register', 'seowave'); ?>"><?php _e('Login / Register', 'seowave'); ?></a>
										<?php
									}
									if ( is_user_logged_in() ) {
										?>
										<?php _e('/', 'seowave'); ?>
										<a href="<?php echo wp_logout_url( get_permalink( wc_get_page_id( 'myaccount' ) ) ); ?>" title="<?php _e('Logout', 'seowave'); ?>"><?php _e('Logout', 'seowave'); ?></a>
										<?php
									}
								}
							?>
						</li>
					</ul>
				</div>
			</div>
		</div>
		
		<!--Top Row-->
		<div class="logo_row container-fluid">
			<div class="container">
				<div class="logo-block">
					<div class="col-sm-6"> 
						<?php
							if( seowave_checkstring( $ow_option['opt_site_logo']['url'] ) && $ow_option['opt_logo_select'] == '2' ):
								?>
								<a class="logo" href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo esc_url( $ow_option['opt_site_logo']['url'] ); ?>" alt=""/></a>
								<?php
							else:
								?>
								<a class="logo" href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php echo get_bloginfo('title'); ?></a>
								<?php
							endif;
						?>
					</div>
					<div class="col-sm-6">
						<ul class="nav">
							<?php if(seowave_checkstring( $ow_option["opt_header_phone"] ) ) { 
								?>
								<li>
									<a href="#">
										<span class="icon phone"></span>
										<span class="first-word"><?php _e( 'Call','seowave' ); ?></span>: <?php echo esc_attr( $ow_option["opt_header_phone"] ); ?>
									</a>
								</li>
								<?php
							}
							
							if(seowave_checkstring( $ow_option["opt_header_email"] ) ) { 
								?>
								<li>
									<a href="mailto:<?php echo esc_attr( $ow_option["opt_header_email"] ); ?>">
										<span class="icon email"></span>
										<span class="first-word"><?php _e('Email','seowave'); ?></span>: <?php echo esc_attr( $ow_option["opt_header_email"] ); ?>
									</a>
								</li>
								<?php
							}
							?>
						</ul>
					</div>
				</div>
			</div>
		</div>
		
		<!--Logo Row-->
		<nav class="menu_row">
			<div class="container">
				<div class="row">
					<div class="search_form_area col-md-5 no-padding">
						<?php
							if( seowave_checkstring( $ow_option["opt_header_link_url"] ) ) : 
								?>
								<div class="col-md-6 col-sm-6 col-xs-6 no-padding"><?php get_search_form() ?></div>
								<a href="<?php echo esc_url( $ow_option["opt_header_link_url"] ); ?>" class="col-sm-6 col-sm-6 col-xs-6 free_analysis">
									<img src="<?php echo esc_url( IMG_URI ); ?>/icons/menu/free_analysis.png" alt=""> 
									<?php echo esc_attr( $ow_option["opt_header_link"] ); ?>
								</a>
								<?php
							else :
								?> <div class="col-md-12 col-sm-12 no-padding"><?php get_search_form() ?></div> <?php
							endif;	
						?>
					</div>
					<!--Search And Analysis-->
					<div class="menu_area col-md-7 no-padding">
						<button class="btn btn-primary visible-xs" type="button" data-toggle="collapse" data-target="#mainMenu" aria-expanded="false" aria-controls="mainMenu"> 
							<i class="glyphicon glyphicon-th-large"></i> 
							 Menu
							  <?php // _e('Menu','seowave'); ?>  
						</button>
						<div class="collapse navbar-collapse no-left-padding" id="mainMenu">
							<?php 
							if( has_nav_menu('primary') ) :
								wp_nav_menu( array(
									'theme_location' => 'primary',
									'container' => false,
									'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>',
									'menu_class' => 'nav navbar-nav',
									'depth' => 10 ,
									'walker' => new ow_nav_walker
								));
							endif;
							?>
						</div>
					</div>
				<!--Menu Area--> 
				</div>
			</div>
		</nav>
	<!--Menu Row--> 
	</header>
	<?php
		if( seowave_checkstring( get_post_meta( get_the_ID(), 'ow_cf_page_header_img', true ) ) ) {
			$header_image = get_post_meta( get_the_ID(), 'ow_cf_page_header_img', true );
		}
		else {
			$header_image = IMG_URI . '/page-cover.jpg';
		}
		if( get_post_meta( get_the_ID(), 'ow_cf_page_title', true ) != "disable" && !is_home() ) :
			?>
			<div class="page-banner"<?php if( seowave_checkstring( $header_image ) ) { ?> style="background-image: url(<?php echo esc_url( $header_image ); ?>);"<?php } ?>>
				<div class="page-heading">
					<div class="container">
						<div class="page-title pull-left no-padding">
							<h3>
								<?php
								if( is_404() ) {
									_e( 'Error Page', 'seowave' );
								}
								elseif( is_search() ) {
									printf( __( 'Search Results for: %s', 'seowave' ), get_search_query() );
								}
								elseif( is_archive() ) {
									the_archive_title();
								}
								else {
									the_title();
								}
								if( seowave_checkstring( get_post_meta( get_the_ID(), 'ow_cf_page_sub_title', true ) ) ) {
									?>
									<span>
										<?php echo esc_attr( get_post_meta( get_the_ID(), 'ow_cf_page_sub_title', true ) ); ?>
									</span>
									<?php
								}
								?>
							</h3>
						</div>
						<?php
							if( seowave_checkstring ( get_post_meta( get_the_ID(), 'ow_cf_page_breadcrumb', true)) && ( get_post_meta( get_the_ID(), 'ow_cf_page_breadcrumb', true)) == 'enable' ) {
								?>
								<div id="page-breadcrumb" class="page-breadcrumb">
									<ol class="breadcrumb breadcrumb-inner m_b">
										<?php
										if( function_exists( 'bcn_display' ) ) {
											bcn_display();
										}
										?>
									</ol>
								</div>
								<?php
							}
						?>
					</div>
				</div>
			</div>
			<?php 
		endif; 
	?>

			