<?php
/* Header Settings */
Redux::setSection( $opt_name, array(
	'title'  => __( 'Header', 'seowave' ),
	'id'    => 'header_settings',
	'icon'  => 'el el-credit-card',
	'subsection' => false,
	'fields'     => array(
		/* Fields */

		/* Site Logo */
		array(
			'id'=>'info_logo',
			'type' => 'info',
			'title' => 'Logo',
		),
		array(
			'id'       => 'opt_logo_select',
			'type'     => 'select',
			'title'    => __( 'Logo Type', 'seowave' ),
			'options'  => array(
				'1' => 'Text Logo',
				'2' => 'Image Logo',
			),
			'default'  => '2',
		),
		array(
			'id'=>'opt_site_logo',
			'type' => 'media',
			'title' => __('Logo Upload', 'seowave' ),
			'required' => array( 'opt_logo_select', '=', '2' ),
			'default' => array( 'url' => esc_url( IMG_URI ) . '/common/logo.png' ),
		),

		/* Header Top */
		array(
			'id'=>'info_header_top',
			'type' => 'info',
			'title' => 'Header Top',
		),		
		
		array(
			'id'=>'opt_header_left',
			'type' => 'text',
			'title' => __('Left Text', 'seowave'),
			'default'  => __('best digital marketing theme forever! ', 'seowave')
		),
		
		array(
			'id'=>'opt_header_phone',
			'type' => 'text',
			'title' => __('Contact no', 'seowave'),
			'default'  => __('+(61) 123 456 7890', 'seowave')
		),
		
		array(
			'id'=>'opt_header_email',
			'type' => 'text',
			'title' => __('Contact Email', 'seowave'),
			'default'  => __('info@example.com', 'seowave')
		),
		
		/* Header Top */
		array(
			'id'=>'info_header',
			'type' => 'info',
			'title' => 'Header',
		),
		
		array(
			'id'=>'opt_header_link',
			'type' => 'text',
			'title' => __('Link Text', 'seowave'),
			'default'  => __('free seo analysis', 'seowave')
		),
		
		array(
			'id'=>'opt_header_link_url',
			'type' => 'text',
			'title' => __('Link Text URL', 'seowave'),
			'default'  => __('#', 'seowave')
		),
		
		
		/* Fields /- */
	)		
) );
/* Header Settings /- */
?>