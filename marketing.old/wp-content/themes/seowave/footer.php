<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Seowave
 * @since Seowave 1.0
 */
	global $ow_option;
?>
	<footer class="row">
		<div class="m0 top_footer">
			<div class="container">
				<div class="row footer_widgets">
					<?php 
					if ( is_active_sidebar( 'sidebar-5' ) ) {
						?>
						<div class="col-sm-4 footer_widget">
							<?php dynamic_sidebar('sidebar-5'); ?>
						</div>
						<?php
					}
					
					if ( is_active_sidebar( 'sidebar-6' ) ) {
						?>
						<div class="col-sm-4 footer_widget">
							<?php dynamic_sidebar('sidebar-6'); ?>
						</div>
						<?php
					}
					
					if ( is_active_sidebar( 'sidebar-7' ) ) {
						?>
						<div class="col-sm-4 footer_widget">
							<?php dynamic_sidebar('sidebar-7'); ?>
						</div>
						<?php
					} ?>

				</div>
				
				<div class="row footer_widgets2 m0">
					<ul class="nav nav-justified">
						<?php
						if(seowave_checkstring( $ow_option["opt_footer_phone"] ) ) { 
							?>
							<li><p><img src="<?php echo esc_url( IMG_URI ); ?>/icons/footer/call.png" alt=""><strong><?php _e('Call: ','seowave'); ?></strong><?php echo esc_attr( $ow_option["opt_footer_phone"] ); ?></p></li>
							<?php
						}
						
						if(seowave_checkstring( $ow_option["opt_footer_email"] ) ) { 
							?>
							<li><a href="<?php echo esc_attr( $ow_option["opt_footer_email"] ); ?>"> <img src="<?php echo esc_url( IMG_URI ); ?>/icons/footer/mail.png" alt=""><strong><?php _e('Email: ','seowave'); ?></strong><?php echo esc_attr( $ow_option["opt_footer_email"] ); ?></a></li>
							<?php
						}
						
						if(seowave_checkstring( $ow_option["opt_footer_fax"] ) ) { 
							?>
							<li><p><img src="<?php echo esc_url( IMG_URI ); ?>/icons/footer/fax.png" alt=""><strong><?php _e('Fax: ','seowave'); ?></strong><?php echo esc_attr( $ow_option["opt_footer_fax"] ); ?></p></li>
							<?php
						} ?>
					</ul>
				</div>
				
			</div>
		</div>
		<div class="row copyright m0"><?php echo wpautop( $ow_option["opt_footer_copyright"] ); ?></div>
		<script type="text/javascript" async src="https://d335luupugsy2.cloudfront.net/js/loader-scripts/7aa88f68-1a70-4ff0-a210-bf6b00367c4f-loader.js" ></script>
	</footer>
	<?php wp_footer(); ?>

</body>
</html>