<?php
/**
* The Template for displaying all single posts
*
* @package WordPress
* @subpackage Seowave
* @since Seowave 1.0
*/
get_header(); ?>

<main id="main" class="site-main section-100px">

	<div class="page-content notfound_template">

		<div id="error-page-section" class="error-page-section">

			<div class="container">
				<div class="page_404">
					<div class="col-md-6 col-sm-6 error-content">
						<img src="<?php echo esc_url( IMG_URI ); ?>/404/1.png" alt="1.png"/>
						<div class="widget_search-inner">
						</div>
					</div>
					
					<div class="col-md-6 col-sm-6 text-center">
						<img src="<?php echo esc_url( IMG_URI ); ?>/404/2.png" alt="2.png"/>
						<h2><?php _e('Sorry! the page not found','seowave'); ?></h2>
						<p><?php _e('The Link You Folowed Probably Broken, or the page has been removed.','seowave'); ?></p>
						<a class="return_home borderred_link" href="<?php echo esc_url( home_url( '/' ) ); ?>"><span><?php _e( 'Return to home', 'seowave');?></span></a>
					</div>
					
				</div>

			</div>

		</div>

	</div><!-- Page Content /- -->

</main><!-- .site-main -->

<?php get_footer(); ?>