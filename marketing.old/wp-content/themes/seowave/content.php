<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage Seowave
 * @since Seowave 1.0
 */
?>
<article id="post-<?php the_ID(); ?>" <?php post_class( ); ?>>

	<?php 
		if( is_single() ) :
			?>
			<div class="blog-single">
				<?php
				$post_format = get_post_format();
				if( $post_format == "gallery" && count( get_post_meta( get_the_ID(), 'ow_cf_post_gallery', 1 ) ) > 0 && is_array( get_post_meta( get_the_ID(), 'ow_cf_post_gallery', 1 ) ) ) {
					?>
					<div id="blog-carousel-<?php echo the_ID(); ?>" class="carousel slide" data-ride="carousel">
						<div class="carousel-inner" role="listbox">
							<?php
							$active=1;
							foreach ( (array) get_post_meta( get_the_ID(), 'ow_cf_post_gallery', 1 ) as $attachment_id => $attachment_url ) {
								?>
								<div class="item<?php if( $active == 1 ) { echo ' active'; } ?>">
									<?php echo wp_get_attachment_image( $attachment_id, 'seowave-846-380' ); ?>
								</div>
								<?php
								$active++;
							} ?>
						</div>
						<a title="Previous" class="left carousel-control" href="#blog-carousel-<?php echo the_ID(); ?>" role="button" data-slide="prev">
							<span class="fa fa-chevron-left" aria-hidden="true"></span>
						</a>
						<a title="Next" class="right carousel-control" href="#blog-carousel-<?php echo the_ID(); ?>" role="button" data-slide="next">
							<span class="fa fa-chevron-right" aria-hidden="true"></span>
						</a>
					</div>
					<?php
				}
				if( has_post_thumbnail() ) {
					?>
					<div class="entry-cover">
						<?php the_post_thumbnail('seowave-846-380'); ?>
						<ul class="blog-entry-meta">
							<li class="entry-date">
								<img src="<?php echo esc_url( IMG_URI ); ?>/icons/blog/1.png" alt="">
								<span><?php echo get_the_date(); ?></span>
							</li>
							
							<li class="total-comment">
								<img src="<?php echo esc_url( IMG_URI ); ?>/icons/blog/2.png" alt="">
								<span><?php printf( _nx( 'One Comment', '%1$s Comments', get_comments_number(), 'comments title', 'seowave' ), number_format_i18n( get_comments_number() ) );?></span>
							</li>
							
							<li class="author">
								<img src="<?php echo esc_url( IMG_URI ); ?>/icons/blog/3.png" alt="">
								<?php echo _e( 'By', 'seowave' ); ?> <a href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?> ">
								<?php the_author() ?></a>
							</li>
						</ul>
					</div>
					<?php
				}
				else {
					// Do nothing...
				}
				 ?>
				<!--======= IBLOG INFO =========-->
				<div class="blog-list-content">
					<?php
						if( seowave_checkstring( get_post_meta( get_the_ID(), 'ow_cf_post_icon_id', true ) ) ) :
							?>
							<div class="post-icon">
							<?php
								echo wp_get_attachment_image( get_post_meta( get_the_ID(), 'ow_cf_post_icon_id', true), 'seowave-26-26' );
								
							?>
							</div>
							<?php
						endif;
						the_title( '<h5 class="entry-title">', '</h5>' ); 
					?>
					<div class="entry-content">
						<div class="entry-content-inner">
						<?php
							if( is_single() ) {
								/* translators: %s: Name of current post */
								the_content( sprintf(
									__( 'Continue reading %s', 'seowave' ),
									the_title( '<span class="screen-reader-text">', '</span>', false )
								) );

								wp_link_pages( array(
									'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'seowave' ) . '</span>',
									'after'       => '</div>',
									'link_before' => '<span>',
									'link_after'  => '</span>',
									'pagelink'    => '<span class="screen-reader-text">' . __( 'Page', 'seowave' ) . ' </span>%',
									'separator'   => '<span class="screen-reader-text">, </span>',
								) );
							}
							else {
								the_excerpt();
							}
						?>
						</div>
					</div>
				</div> 
				<div class="shares">
					<div class="share_count pull-left"><?php _e('Shares','seowave'); ?></div>
					<ul class="pull-right share_icon list-inline social-share">
						<li class="facebook"><a href="javascript: void(0)" data-action="facebook" data-title="<?php the_title(); ?>" data-url="<?php echo esc_url(the_permalink()); ?>"><i class="fa fa-facebook"></i></a></li>
						<li class="twitter"><a href="javascript: void(0)" data-action="twitter" data-title="<?php the_title(); ?>" data-url="<?php echo esc_url(the_permalink()); ?>"><i class="fa fa-twitter"></i></a></li>
						<li class="googleplus"><a href="javascript: void(0)" data-action="google-plus" data-url="<?php echo esc_url(the_permalink()); ?>"><i class="fa fa-google-plus"></i></a></li>
						<li class="linkedin"><a href="javascript: void(0)" data-action="linkedin" data-title="<?php the_title(); ?>" data-url="<?php echo esc_url(the_permalink()); ?>"><i class="fa fa-linkedin"></i></a></li>
						<li class="soundcloud"><a href="javascript: void(0)" data-action="rss" data-title="<?php the_title(); ?>" data-url="<?php echo esc_url(the_permalink()); ?>"><i class="fa fa-rss"></i></a></li>
					</ul>
                </div>
				<!--======= TAGS =========-->
				<div class="tags entry-tag hide">
					<?php the_tags(); ?>
				</div>
			</div>
				<?php
				// Previous/next post navigation.
				the_post_navigation( array(
					'next_text' => '<span class="meta-nav" aria-hidden="true">' . __( 'Next ', 'seowave' ) . '</span> ' .
						'<span class="screen-reader-text">' . __( 'Next post:', 'seowave' ) . '</span> ' .
						'<span class="post-title">%title</span>',
					'prev_text' => '<span class="meta-nav" aria-hidden="true">' . __( ' Previous', 'seowave' ) . '</span> ' .
						'<span class="screen-reader-text">' . __( 'Previous post:', 'seowave' ) . '</span> ' .
						'<span class="post-title">%title</span>',
				) );
			else:
				?>
				<!--======= BLOG POST =========-->
				<div class="blog-post">
					<div class="entry-cover">
						<?php the_post_thumbnail('seowave-817-391'); ?>
						<ul class="blog-entry-meta">
							<li class="entry-date">
								<img src="<?php echo esc_url( IMG_URI ); ?>/icons/blog/1.png" alt="">
								<a href="<?php echo esc_url( get_permalink() ); ?>"><?php echo get_the_date(); ?></a>
							</li>
							
							<li class="total-comment">
								<img src="<?php echo esc_url( IMG_URI ); ?>/icons/blog/2.png" alt="">
								<a href="<?php echo esc_url( get_permalink() ); ?>"><?php printf( _nx( 'One Comment', '%1$s Comments', get_comments_number(), 'comments title', 'seowave' ), number_format_i18n( get_comments_number() ) );?></a>
							</li>
							
							<li class="author">
								<img src="<?php echo esc_url( IMG_URI ); ?>/icons/blog/3.png" alt="">
								<?php echo _e( 'By', 'seowave' ); ?> <a href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?> ">
								<?php the_author() ?></a>
							</li>
						</ul>
					</div>
					<div class="blog-list-content">
						<?php
							if( seowave_checkstring( get_post_meta( get_the_ID(), 'ow_cf_post_icon_id', true ) ) ) :
								?>
								<div class="post-icon">
								<?php
									echo wp_get_attachment_image( get_post_meta( get_the_ID(), 'ow_cf_post_icon_id', true), 'seowave-26-26' );
									
								?>
								</div>
								<?php
							endif;
						?>
						<h5 class="entry-title"><a href="<?php echo esc_url( get_permalink() ); ?>"><?php the_title() ?></a></h5>
						<div class="entry-content">							
						
					<?php the_excerpt('<p>','</p>' ); ?>
					<a class="read_more" href="<?php echo esc_url( get_permalink() ); ?>"><?php _e( 'Saiba Mais', 'seowave' ); ?></a>
					</div>
					</div>
				</div>
				<?php
				$postformate = get_post_format();
				if($postformate == "quote") {
					if( seowave_checkstring( get_post_meta( get_the_ID(), 'ow_cf_post_author', true ) ) ) {
					?>
					<div class="quote-post">
						<?php 
							echo seowave_content('<p class="font-lora">','</p>', esc_html( get_post_meta( get_the_ID(), 'ow_cf_post_quote', true ) ) );
							echo seowave_content('<a href="'.esc_url(get_post_meta( get_the_ID(), 'ow_cf_post_author_link', true ) ).'">&mdash; ','</a>', esc_html( get_post_meta( get_the_ID(), 'ow_cf_post_author', true ) ) ); 
						?>	
					</div>
				<?php
				}
				}
			endif;
		?>
</article>
<div class="clearfix"></div>