<?php
require_once( trailingslashit( get_template_directory() ) . 'include/common.php' );
require_once( trailingslashit( get_template_directory() ) . 'include/inc.php' );
require_once( trailingslashit( get_template_directory() ) . 'admin/inc.php' );


/**
 * Set up the content width value based on the theme's design.
 *
 * @see ow_content_width()
 *
 * @since Seowave 1.0
 */
if ( ! isset( $content_width ) ) { $content_width = 474; }

/**
 * Adjust content_width value for image attachment template.
 *
 * @since Seowave 1.0
 */
if( !function_exists('ow_content_width') ) :
	function ow_content_width() {
		if ( is_attachment() && wp_attachment_is_image() ) { $GLOBALS['content_width'] = 810; }
	}
	add_action( 'template_redirect', 'ow_content_width' );
endif;

/**************************************************************************/

/**
 * Theme setup.
 *
 * Set up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support post thumbnails.
 *
 * @since Seowave 1.0
 */
if( !function_exists('ow_theme_setup') ) :

	function ow_theme_setup() {

		/* load theme languages */
		load_theme_textdomain( 'seowave', get_template_directory() . '/languages' );

		/* Image Sizes */
		set_post_thumbnail_size( 817, 391, true ); /* Default Featured Image */
		
		add_image_size( 'seowave-26-26', 26, 26, true ); // Blog Post Icon
		add_image_size( 'seowave-360-368', 360, 368, true ); // Services Single

		// This theme uses wp_nav_menu() in two locations.
		register_nav_menus( array(
			'primary'   => __( 'Primary menu', 'seowave' ),
		) );

		add_theme_support( 'automatic-feed-links' );

		add_theme_support( 'title-tag' );
		add_theme_support( 'post-thumbnails' );
		
		/* Woocommerce Theme Support */
		add_theme_support( 'woocommerce' );
		add_theme_support( 'wc-product-gallery-zoom' );
		add_theme_support( 'wc-product-gallery-lightbox' );
		add_theme_support( 'wc-product-gallery-slider' );
	
		add_theme_support( 'html5', array( 'search-form', 'comment-form', 'comment-list', 'gallery', 'caption' ) );

		/*
		 * Enable support for Post Formats.
		 *
		 * See: https://codex.wordpress.org/Post_Formats
		 */
		add_theme_support( 'post-formats', array( 'quote','gallery') );
	}
	add_action( 'after_setup_theme', 'ow_theme_setup' );
endif;

/* ************************************************************************ */

if ( ! function_exists( 'seowave_fonts_url' ) ) :
/**
 * Register Google fonts for Twenty Sixteen.
 *
 * Create your own seowave_fonts_url() function to override in a child theme.
 *
 * @since Seowave 1.0
 *
 * @return string Google fonts URL for the theme.
 */
function seowave_fonts_url() {

	$fonts_url = '';
	$fonts     = array();
	$subsets   = 'latin,latin-ext';

	/* translators: If there are characters in your language that are not supported by Montserrat, translate this to 'off'. Do not translate into your own language. */
	if ( 'off' !== _x( 'on', 'Montserrat font: on or off', 'seowave' ) ) {
		$fonts[] = 'Montserrat:400,700';
	}

	if ( 'off' !== _x( 'on', 'Droid Serif font: on or off', 'seowave' ) ) {
		$fonts[] = 'Droid Serif:400,700,400italic,700italic';
	}

	if ( 'off' !== _x( 'on', 'Cabin font: on or off', 'seowave' ) ) {
		$fonts[] = 'Cabin:400,500,600,700,400italic,500italic,600italic,700italic';
	}

	if ( $fonts ) {
		$fonts_url = add_query_arg( array(
			'family' => urlencode( implode( '|', $fonts ) ),
			'subset' => urlencode( $subsets ),
		), 'https://fonts.googleapis.com/css' );
	}

	return $fonts_url;
}
endif;

/**************************************************************************/

/**
 * Enqueue scripts and styles for the front end.
 *
 * @since Seowave 1.0
 */
if( !function_exists('ow_enqueue_scripts') ) :

	function ow_enqueue_scripts() {

		// Add custom fonts, used in the main stylesheet.
		wp_enqueue_style( 'seowave-fonts', seowave_fonts_url() );

		// load the Internet Explorer specific stylesheet.
		wp_enqueue_style( 'ie-css', get_template_directory_uri() . '/css/ie.css' );
		wp_style_add_data( 'ie-css', 'conditional', 'lt IE 9' );

		if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
			wp_enqueue_script( 'comment-reply' );
		}
		
		// Load the html5 shiv.
		wp_enqueue_script( 'respond-min', get_template_directory_uri() . '/js/html5/respond.min.js', array('jquery') );
		wp_script_add_data( 'respond-min', 'conditional', 'lt IE 9' );

		/* Font Icons */
		wp_enqueue_style( 'dashicons' );
		wp_enqueue_style( 'genericons', get_template_directory_uri() . '/fonts/genericons.css' );
		wp_enqueue_style( 'font-awesome-min', get_template_directory_uri() . '/css/font-awesome.min.css' );

		wp_enqueue_script( 'counterup-min', get_template_directory_uri() . '/vendors/counterup/jquery.counterup.min.js', array('jquery') );
		wp_enqueue_script( 'waypoints-min', get_template_directory_uri() . '/vendors/waypoints/waypoints.min.js');
		wp_enqueue_script( 'isotope-custom', get_template_directory_uri() . '/vendors/isotope/isotope-custom.js');

		wp_enqueue_style( 'bootstrap-min', get_template_directory_uri() . '/css/bootstrap.min.css');
		wp_enqueue_script( 'bootstrap-min', get_template_directory_uri() . '/js/bootstrap.min.js');

		wp_enqueue_style( 'owlcarousel-min', get_template_directory_uri() . '/vendors/owl.carousel/css/owl.carousel.min.css');
		wp_enqueue_style( 'owlcarousel-theme', get_template_directory_uri() . '/vendors/owl.carousel/css/owl.theme.default.min.css');
		wp_enqueue_script( 'owlcarousel-min', get_template_directory_uri() . '/vendors/owl.carousel/js/owl.carousel.min.js');

		wp_enqueue_style( 'flexslider', get_template_directory_uri() . '/vendors/flexslider/flexslider.css');
		wp_enqueue_script( 'flexslider-min', get_template_directory_uri() . '/vendors/flexslider/jquery.flexslider-min.js');

		wp_enqueue_style( 'seowave-woocommerce', get_template_directory_uri() . '/css/woocommerce.css');		
		wp_enqueue_style( 'seowave-stylesheet', get_template_directory_uri() . '/style.css' );
		wp_enqueue_style( 'seowave-responsive-style', get_template_directory_uri() . '/css/responsive.css');
		
		wp_enqueue_style( 'seowave-theme-style', get_template_directory_uri() . '/css/theme.css');
		wp_enqueue_script( 'seowave-theme', get_template_directory_uri() . '/js/theme.js' );
	}
	add_action( 'wp_enqueue_scripts', 'ow_enqueue_scripts' );
endif;

function to_slug($string){
    return strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '_', $string)));
}

/**************************************************************************/

/**
 * Extend the default WordPress body classes.
 *
 * @since Seowave 1.0
 *
 * @param array $classes A list of existing body class values.
 * @return array The filtered body class list.
 */
if( !function_exists('ow_body_classes') ) :

	function ow_body_classes( $classes ) {

		if ( is_singular() && ! is_front_page() ) {
			$classes[] = 'singular';
		}

		if( is_front_page() && !is_home() ) {
			$classes[] = 'front-page';
		}

		$classes[] = 'default';

		return $classes;
	}
	add_filter( 'body_class', 'ow_body_classes' );
endif;

/**
 * Extend the default WordPress post classes.
 *
 * Adds a post class to denote:
 * Non-password protected page with a post thumbnail.
 *
 * @since Seowave 1.0
 *
 * @param array $classes A list of existing post class values.
 * @return array The filtered post class list.
 */
if( !function_exists('ow_post_classes') ) :
	function ow_post_classes( $classes ) {
		if ( ! is_attachment() && has_post_thumbnail() ) { $classes[] = 'has-post-thumbnail'; }
		return $classes;
	}
	add_filter( 'post_class', 'ow_post_classes' );
endif;

/**
 * Add a `screen-reader-text` class to the search form's submit button.
 *
 * @since Seowave 1.0
 *
 * @param string $html Search form HTML.
 * @return string Modified search form HTML.
 */
function ow_search_form_modify( $html ) {
	$html = '<form role="search" method="get" id="searchform" class="searchform" action="' . home_url( '/' ) . '" >
	<div class="input-group">
	<input type="text" name="s" id="s" placeholder="Search Post Here..." class="form-control" required>
	<span class="input-group-btn">
		<button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
	</span>
	</div><!-- /input-group -->
	</form>';
	return $html;
}
add_filter( 'get_search_form', 'ow_search_form_modify' );
?>