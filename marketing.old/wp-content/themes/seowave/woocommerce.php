<?php
/**
* The Template for displaying all single posts
*
* @package WordPress
* @subpackage Seowave
* @since Seowave 1.0
*/

get_header();

global $ow_option;
?>
<main id="main" class="site-main section-100px" role="main">

	<div class="page-content">

		<?php
		if( wc_get_page_id('shop') && !is_singular('product') ) {

			/* Page Layout */
			$page_layout = get_post_meta( wc_get_page_id('shop'), 'ow_cf_page_layout', true );

			/* Widget Area */
			$sidebar_layout = get_post_meta( wc_get_page_id('shop'), 'ow_cf_sidebar_layout', true );

			if( $sidebar_layout == "right_sidebar" ) {
				$sidebar_css = "sidebar-right";
			}
			elseif( $sidebar_layout == "left_sidebar" ) {
				$sidebar_css = "sidebar-left";
			}
			elseif( $sidebar_layout == "no_sidebar" ) {
				$sidebar_css = "no-sidebar";
			}
			else {
				$sidebar_css = "sidebar-right";
			}

			/* Content Area */
			if( $sidebar_layout == "right_sidebar" ) {
				$content_area_css = "content-left col-md-9 col-sm-8";
			}
			elseif( $sidebar_layout == "left_sidebar" ) {
				$content_area_css = "content-right col-md-9 col-sm-8";
			}
			elseif( $sidebar_layout == "no_sidebar" ) {
				$content_area_css = "full-content col-md-12";
			}
			else {
				$content_area_css = "col-md-9 col-sm-8";
			}

			$widget_area = get_post_meta( wc_get_page_id('shop'), 'ow_cf_widget_area', true );
			?>
			<div class="container">

				<!-- Content Area -->
				<div class="content-area container-shopping <?php echo esc_attr( $content_area_css ); ?>">
					<?php woocommerce_content(); ?>
				</div>

				<!-- Sidebar -->
				<?php
				if( $sidebar_layout != "no_sidebar" && is_active_sidebar( $widget_area ) ) {
					?>
					<div class="widget-area col-md-3 col-sm-4 sidebar-shop <?php echo esc_attr( $sidebar_css ); ?>">
						<div class="sidebar-inner">
							<?php dynamic_sidebar( $widget_area ); ?>
						</div><!-- .sidebar-inner -->
					</div><!-- .widget-area -->
					<?php
				}
				?>
			</div>
			<?php
		}
		else {

			/* Sidebar CSS */
			if( $ow_option['opt_wc_sidebar'] != 0 ) {
				$content_area_css = "col-md-9 col-sm-8 container-shopping";
			}
			else {
				$content_area_css = "col-md-12 no-padding container-shopping";
			}
			?>
			<div class="container no-padding">

				<!-- Content Area -->
				<div class="content-area container-shopping <?php echo esc_attr( $content_area_css ); ?>">
					<?php woocommerce_content(); ?>
				</div>

				<!-- Sidebar -->
				<?php
				if( $ow_option['opt_wc_sidebar'] != 0 ) {
					?>
					<div class="widget-area sidebar sidebar-shop col-md-3 col-sm-4">
						<div class="sidebar-inner">
							<?php dynamic_sidebar( $ow_option['opt_wc_widget_area'] ); ?>
						</div><!-- .sidebar-inner -->
					</div><!-- .widget-area -->
					<?php
				}
				?>
			</div>
			<?php
		} ?>

	</div><!-- Page Content /- -->

</main><!-- .site-main -->

<?php get_footer(); ?>